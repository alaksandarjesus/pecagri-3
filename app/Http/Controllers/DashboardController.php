<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Forms\FormD;
use App\Models\Forms\FormII;

class DashboardController extends Controller
{
    public function view(Request $request){

        $user = $request->user;

        $forms = (object)[];
        $forms->formd = $this->getFormDInformation($user);
        $forms->formii = $this->getFormIIInformation($user);



    
        return view('dashboard.index')->withForms($forms)->withUser($user);
    }

    private function getFormDInformation($user){
        $count = 0;
        $can_process_form_d = $user->can_process_form_d;
        $can_apply_form_d = $user->can_apply_form_d;

        if($can_apply_form_d || $can_process_form_d){
            $count = FormD::orderBy('updated_at', 'desc')
                                ->when($can_apply_form_d, function($query) use($user){
                                    return $query->where('created_by', $user->id);
                                })->count();
        }
        return $count;
    }

    private function getFormIIInformation($user){
        $count = 0;
        $can_process_form_ii = $user->can_process_form_ii;
        $can_apply_form_ii = $user->can_apply_form_ii;

        if($can_apply_form_ii || $can_process_form_ii){
            $count = FormII::orderBy('updated_at', 'desc')
                                ->when($can_apply_form_ii, function($query) use($user){
                                    return $query->where('created_by', $user->id);
                                })->count();
        }
        return $count;
    }
}
