<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Forms\FormA1;
use App\Http\Requests\Forms\FormA1Request;
use Illuminate\Support\Str;
use App\Models\Forms\FormA1Process;

class FormA1Controller extends Controller
{
    public function list(Request $request){
        $user = $request->user;
        $can_process_form_a1 = $user->can_process_form_a1;
        $can_apply_form_a1 = $user->can_apply_form_a1;
        $forms = [];
        if($can_apply_form_a1 || $can_process_form_a1){
            $forms = FormD::orderBy('updated_at', 'desc')
                                ->when($can_apply_form_d, function($query) use($user){
                                    return $query->where('created_by', $user->id);
                                })->get();
        }
        return view('form-a1.list')->withForms($forms);
    }
   
    public function view($uuid, Request $request){
        $form = FormA1::where('uuid', $uuid)
        ->first();
        if(!$form->can_view){
            return redirect()->to('404');
        }
        return view('form-a1.view')->withForm($form);

    }
    
    public function create(){
        return view('form-a1.form');
    }

    public function edit($uuid, Request $request){
        $form = FormA1::where('uuid', $uuid)
                ->where('created_by', $request->user->id)
                ->where('edit_required', 1)
                ->first();
        if(empty($form)){
            return redirect()->to('404');
        }
        return view('form-a1.form')->withForm($form);
    }

    public function store(FormA1Request $request){

        $validated = (object) $request->validated();

        $row = FormA1::where('uuid', $validated->uuid)->first();

        if(empty($row)){
            $row = new FormA1;
            $row->created_by = $request->user->id;
            $row->uuid = Str::uuid();
        } 

        $row->name = $validated->name;
        $row->concern = $validated->concern;
        $row->address = $validated->address;
        $row->telephone = $validated->telephone;
        $row->saleAddress = $validated->saleAddress;
        $row->storageAddress = $validated->storageAddress;
        $row->fertilizerName = $validated->fertilizerName;
        $row->certifiateSource = $validated->certifiateSource;
        $row->fertilizerName1 = $validated->fertilizerName1;
        $row->certifiateSource1 = $validated->certifiateSource1;
        $row->renewal = $validated->renewal;
        $row->documents = $validated->documents;
        $row->relevantInfo = $validated->relevantInfo;
        $row->application_place = $validated->application_place;
        $row->date = $validated->date;
        $row->signature = $validated->signature;
        $row->place = $validated->place;
        $row->signature1 = $validated->signature1;
        $row->updated_by = $request->user->id;
        $row->edit_required = 0;
        $row->save();

        $process = new FormA1Process;
        $process->form_a1_id = $row->id;
        $process->save();

        $url = url('/dashboard');

        return response()->json(['redirect' => $url]);
    }
}
