<?php

namespace App\Http\Controllers;

use App\Http\Requests\Forms\FormA1ProcessRequest;
use App\Models\Forms\FormD;
use Carbon\Carbon;

class FormA1ProcessController extends Controller
{
    public function process(FormA1ProcessRequest $request)
    {
        $validated = (object) $request->validated();
        $redirect = url('form-a1');
        $form = FormA1::where('uuid', $validated->uuid)->first();
        if ($request->user->is_ao) {
            $form->process->verified_by_ao = Carbon::now();
            $form->process->rejected_by_ao = $validated->reason;
        }

        if ($request->user->is_dda) {
            $form->process->verified_by_dda = Carbon::now();
            $form->process->rejected_by_dda = $validated->reason;
        }

        if ($request->user->is_jda) {
            $form->process->verified_by_jda = Carbon::now();
            $form->process->rejected_by_jda = $validated->reason;
        }

        if ($request->user->is_adaa) {
            $form->process->verified_by_adaa = Carbon::now();
            $form->process->rejected_by_adaa = $validated->reason;
            $redirect = url('form-a1/'. $form->uuid.'/form-a2');
        }


        $form->process->save();

        if ($validated->action === 'edit') {
            $form->edit_required = 1;
            $form->updated_by = $request->user->id;
            $form->save();
        }

        

        return response()->json(['success' => 'true', 'redirect' => $redirect]);

    }
}
