<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Forms\FormA1;
use App\Models\Forms\FormA2;
use App\Http\Requests\Forms\FormA2Request;
use Illuminate\Support\Str;

class FormA2Controller extends Controller
{
    public function create($uuid, Request $request){
        $form = FormA1::where('uuid', $uuid)->first();
        if(empty($form) && !$form->generate_form_a2){
            return redirect()->to('404');
        }
        $issuedFormsCount = FormA1::whereNotNull('form_a2_id')->count();
        $params = (object)[
            'license_number' => str_pad(($issuedFormsCount + 1), 3, '0', STR_PAD_LEFT).'/RS/'.date('Y').'-'.date('+5Y')
        ];
        return view('form-a2.form')->withForm($form)->withParams($params);
    }

    public function store(FormA2Request $request){
        $validated = (object)$request->validated();
        $form = FormA1::where('uuid', $validated->form_a1_uuid)->first();
        if(empty($form) && !$form->generate_form_a2){
            return response()->json(['messages' => 'Unable to process your request'], 422);
        }
        $row = new FormA2;
        $row->uuid = Str::uuid();
        $row->form_a1_id = $form->id;
        $row->concern = $validated->concern;
        $row->challanBearingNo = $validated->challanBearingNo;
        $row->challanDate = $validated->challanDate;
        $row->date = $validated->date;
        $row->license_number = $validated->license_number;
        $row->date_of_issue = $validated->date_of_issue;
        $row->date_of_validity = $validated->date_of_validity;
        $row->signature = $validated->signature;
        $row->created_by = $request->user->id;
        $row->save();
        $form->form_a2_id = $row->id;
        $form->save();
        return response()->json(['success' => true, 'redirect' => url('/form-a1')]);
    }

    public function view($uuid, Request $request){
        $user = $request->user;
        $form = FormA1::where('uuid', $uuid)->first();
        if(empty($form)){
            return redirect()->to('404?empty-form');
        }
        if(!$form->can_view){
            return redirect()->to('404?unauthorized');
        }
        return view('form-a2.view')->withForm($form);
    }

}
