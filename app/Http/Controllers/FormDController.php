<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Forms\FormD;
use App\Http\Requests\Forms\FormDRequest;
use Illuminate\Support\Str;
use App\Models\Forms\FormDProcess;


class FormDController extends Controller
{
    public function list(Request $request){
        $user = $request->user;
        $can_process_form_d = $user->can_process_form_d;
        $can_apply_form_d = $user->can_apply_form_d;
        $forms = [];
        if($can_apply_form_d || $can_process_form_d){
            $forms = FormD::orderBy('updated_at', 'desc')
                                ->when($can_apply_form_d, function($query) use($user){
                                    return $query->where('created_by', $user->id);
                                })->get();
        }
        return view('form-d.list')->withForms($forms);
    }
   
    public function view($uuid, Request $request){
        $form = FormD::where('uuid', $uuid)
        ->first();
        if(!$form->can_view){
            return redirect()->to('404');
        }
        return view('form-d.view')->withForm($form);

    }
    
    public function create(){
        return view('form-d.form');
    }

    public function edit($uuid, Request $request){
        $form = FormD::where('uuid', $uuid)
                ->where('created_by', $request->user->id)
                ->where('edit_required', 1)
                ->first();
        if(empty($form)){
            return redirect()->to('404');
        }
        return view('form-d.form')->withForm($form);
    }

  

    public function store(FormDRequest $request){

        $validated = (object) $request->validated();

        $row = FormD::where('uuid', $validated->uuid)->first();

        if(empty($row)){
            $row = new FormD;
            $row->created_by = $request->user->id;
            $row->uuid = Str::uuid();
        } 

        $row->name = $validated->name;
        $row->address = $validated->address;
        $row->concern = $validated->concern;
        $row->qualification = $validated->qualification;
        $row->new_comer = $validated->new_comer;
        $row->premises = $validated->premises;
        $row->particulars = $validated->particulars;
        $row->certificates = $validated->certificates;
        $row->duration = $validated->duration;
        $row->quantity = $validated->quantity;
        $row->business = $validated->business;
        $row->quantities = $validated->quantities;
        $row->renewal = $validated->renewal;
        $row->requiring = $validated->requiring;
        $row->documents = $validated->documents;
        $row->applicant_name = $validated->applicant_name;
        $row->applicant_address = $validated->applicant_address;
        $row->application_place = $validated->application_place;
        $row->date = $validated->date;
        $row->signature = $validated->signature;
        $row->updated_by = $request->user->id;
        $row->edit_required = 0;
        $row->save();

        $process = new FormDProcess;
        $process->form_d_id = $row->id;
        $process->save();

        $url = url('/dashboard');

        return response()->json(['redirect' => $url]);
    }

}
