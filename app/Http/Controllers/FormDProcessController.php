<?php

namespace App\Http\Controllers;

use App\Http\Requests\Forms\FormDProcessRequest;
use App\Models\Forms\FormD;
use Carbon\Carbon;

class FormDProcessController extends Controller
{
    public function process(FormDProcessRequest $request)
    {
        $validated = (object) $request->validated();
        $redirect = url('form-d');
        $form = FormD::where('uuid', $validated->uuid)->first();
        if ($request->user->is_ao) {
            $form->process->verified_by_ao = Carbon::now();
            $form->process->rejected_by_ao = $validated->reason;
        }

        if ($request->user->is_dda) {
            $form->process->verified_by_dda = Carbon::now();
            $form->process->rejected_by_dda = $validated->reason;
        }

        if ($request->user->is_jda) {
            $form->process->verified_by_jda = Carbon::now();
            $form->process->rejected_by_jda = $validated->reason;
        }

        if ($request->user->is_ada) {
            $form->process->verified_by_ada = Carbon::now();
            $form->process->rejected_by_ada = $validated->reason;
        }

        if ($request->user->is_director) {
            $form->process->verified_by_director = Carbon::now();
            $form->process->rejected_by_director = $validated->reason;
            $redirect = url('form-d/'. $form->uuid.'/form-f');

        }

        $form->process->save();

        if ($validated->action === 'edit') {
            $form->edit_required = 1;
            $form->updated_by = $request->user->id;
            $form->save();
        }

        

        return response()->json(['success' => 'true', 'redirect' => $redirect]);

    }
}
