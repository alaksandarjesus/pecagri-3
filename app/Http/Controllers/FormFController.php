<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Forms\FormD;
use App\Models\Forms\FormF;
use App\Http\Requests\Forms\FormFRequest;
use Illuminate\Support\Str;


class FormFController extends Controller
{
    
    public function create($uuid, Request $request){
        $form = FormD::where('uuid', $uuid)->first();
        if(empty($form) && !$form->generate_form_f){
            return redirect()->to('404');
        }
        $issuedFormsCount = FormD::whereNotNull('form_f_id')->count();
        $params = (object)[
            'certificate_number' => str_pad(($issuedFormsCount + 1), 3, '0', STR_PAD_LEFT).'/PY/Agri/'.date('Y')
        ];
        return view('form-f.form')->withForm($form)->withParams($params);
    }

    public function store(FormFRequest $request){
        $validated = (object)$request->validated();
        $form = FormD::where('uuid', $validated->form_d_uuid)->first();
        if(empty($form) && !$form->generate_form_f){
            return response()->json(['messages' => 'Unable to process your request'], 422);
        }
        $row = new FormF;
        $row->uuid = Str::uuid();
        $row->form_d_id = $form->id;
        $row->name = $validated->name;
        $row->certificate_number = $validated->certificate_number;
        $row->place = $validated->place;
        $row->signature = $validated->signature;
        $row->date_of_issue = $validated->date_of_issue;
        $row->valid_upto = $validated->valid_upto;
        $row->submit_date = $validated->submit_date;
        $row->created_by = $request->user->id;
        $row->save();
        $form->form_f_id = $row->id;
        $form->save();
        return response()->json(['success' => true, 'redirect' => url('/form-d')]);
    }

    public function view($uuid, Request $request){
        $user = $request->user;
        $form = FormD::where('uuid', $uuid)->first();
        if(empty($form)){
            return redirect()->to('404?empty-form');
        }
        if(!$form->can_view){
            return redirect()->to('404?unauthorized');
        }
        return view('form-f.view')->withForm($form);
    }
}
