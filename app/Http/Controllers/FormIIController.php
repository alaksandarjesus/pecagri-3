<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Forms\FormII;
use App\Http\Requests\Forms\FormIIRequest;
use Illuminate\Support\Str;
use App\Models\Forms\FormIIProcess;


class FormIIController extends Controller
{
    public function list(Request $request){
        $user = $request->user;
        $can_process_form_ii = $user->can_process_form_ii;
        $can_apply_form_ii = $user->can_apply_form_ii;
        $forms = [];
        if($can_apply_form_ii || $can_process_form_ii){
            $forms = FormII::orderBy('updated_at', 'desc')
                                ->when($can_apply_form_ii, function($query) use($user){
                                    return $query->where('created_by', $user->id);
                                })->get();
        }
        return view('form-ii.list')->withForms($forms);
    }

    public function view($uuid, Request $request){
        $form = FormII::where('uuid', $uuid)
        ->first();
        if(!$form->can_view){
            return redirect()->to('404');
        }
        return view('form-ii.view')->withForm($form);

    }

    public function create(){
        return view('form-ii.form');
    }

    public function edit($uuid, Request $request){
        $form = FormII::where('uuid', $uuid)
                ->where('created_by', $request->user->id)
                ->where('edit_required', 1)
                ->first();
        if(empty($form)){
            return redirect()->to('404');
        }
        return view('form-ii.form')->withForm($form);
    }

    public function store(FormIIRequest $request){

        $validated = (object) $request->validated();

        $row = FormII::where('uuid', $validated->uuid)->first();

        if(empty($row)){
            $row = new FormII;
            $row->created_by = $request->user->id;
            $row->uuid = Str::uuid();
        } 
        $row->applicantphoto = $validated->applicantphoto;
        $row->name = $validated->name;
        $row->address = $validated->address;
        $row->email = $validated->email;
        $row->premisesAddress = $validated->premisesAddress;
        $row->validity = $validated->validity;
        $row->storeAddress = $validated->storeAddress;
        $row->pestControl = $validated->pestControl;
        $row->residentArea = $validated->residentArea;
        $row->foodArticle = $validated->foodArticle;
        $row->insecticideQualification = $validated->insecticideQualification;
        $row->courseName = $validated->courseName;
        $row->durationCourse = $validated->durationCourse;
        $row->certificateAward = $validated->certificateAward;
        $row->manufactureDetails = $validated->manufactureDetails;
        $row->commercialApplication = $validated->commercialApplication;
        $row->zonalBranchAddress = $validated->zonalBranchAddress;
        $row->premisesLicenceAddress = $validated->premisesLicenceAddress;
        $row->technicalExpertise = $validated->technicalExpertise;
        $row->refApprovalNumber = $validated->refApprovalNumber;
        $row->itsIssueDate = $validated->itsIssueDate;
        $row->itsValidityDate = $validated->itsValidityDate;
        $row->nameRestrict = $validated->nameRestrict;
        $row->techPersonName = $validated->techPersonName;
        $row->insecticideRestrict = $validated->insecticideRestrict;
        $row->particularsQuantity = $validated->particularsQuantity;
        $row->detailsEquipment = $validated->detailsEquipment;
        $row->particularLicence = $validated->particularLicence;
        $row->otherStateApplicant = $validated->otherStateApplicant;
        $row->stateLicenceNumber = $validated->stateLicenceNumber;
        $row->grantDate = $validated->grantDate;
        $row->ApplicationFee = $validated->ApplicationFee;
        $row->documents = $validated->documents;
        $row->relevantInfo = $validated->relevantInfo;
        $row->signature = $validated->signature;
        $row->decName = $validated->decName;
        $row->sonOfname = $validated->sonOfname;
        $row->capacityAs = $validated->capacityAs;
        $row->virtueOf = $validated->virtueOf;
        $row->application_place = $validated->application_place;
        $row->date = $validated->date;
        $row->signature1 = $validated->signature1;
        $row->updated_by = $request->user->id;
        $row->edit_required = 0;
        $row->save();

        $process = new FormIIProcess;
        $process->form_ii_id = $row->id;
        $process->save();

        $url = url('/dashboard');

        return response()->json(['redirect' => $url]);
    }

}