<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Forms\FormII;
use App\Models\Forms\FormIII;
use App\Http\Requests\Forms\FormIIIRequest;
use Illuminate\Support\Str;

class FormIIIController extends Controller
{
    public function create($uuid, Request $request){
        $form = FormII::where('uuid', $uuid)->first();
        if(empty($form) && !$form->generate_form_iii){
            return redirect()->to('404');
        }
        $issuedFormsCount = FormII::whereNotNull('form_iii_id')->count();
        $params = (object)[
            'license_number' => str_pad(($issuedFormsCount + 1), 3, '0', STR_PAD_LEFT).'/M/'.date('Y').'/E'
        ];
        return view('form-iii.form')->withForm($form)->withParams($params);
    }

    public function store(FormIIIRequest $request){
        $validated = (object)$request->validated();
        $form = FormII::where('uuid', $validated->form_ii_uuid)->first();
        if(empty($form) && !$form->generate_form_iii){
            return response()->json(['messages' => 'Unable to process your request'], 422);
        }
        $row = new FormIII;
        $row->uuid = Str::uuid();
        $row->form_ii_id = $form->id;
        $row->name = $validated->name;
        $row->license_number = $validated->license_number;
        $row->address = $validated->address;
        $row->email = $validated->email;
        $row->otherConditions = $validated->otherConditions;
        $row->signature = $validated->signature;
        $row->signature1 = $validated->signature1;
        $row->created_by = $request->user->id;
        $row->save();
        $form->form_iii_id = $row->id;
        $form->save();
        return response()->json(['success' => true, 'redirect' => url('/form-ii')]);
    }

    public function view($uuid, Request $request){
        $user = $request->user;
        $form = FormII::where('uuid', $uuid)->first();
        if(empty($form)){
            return redirect()->to('404?empty-form');
        }
        if(!$form->can_view){
            return redirect()->to('404?unauthorized');
        }
        return view('form-iii.view')->withForm($form);
    }
}
