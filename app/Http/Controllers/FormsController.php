<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Forms\FormII;

class FormsController extends Controller
{
    public function view(Request $request){
        $user = $request->user;
        return view('forms.index')->withUser($user);
    }


}
