<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    
    public function upload($folder, Request $request){
            $file = $request->file('file');
            $filename = $request->get('filename');
            $url = '';
            if(empty($filename)){
                $filename = uniqid().'-'.$file->getClientOriginalName(); 
                $url = Storage::putFileAs('public/'.$folder.'/' . $request->user->uuid, $file, $filename);
            }else{
                $filename = $filename.'.'.$file->extension();
                $url = Storage::putFileAs('public/'.$folder.'/' . $request->user->uuid, $file, $filename);
            }

            return response()->json(['url' => url($url), 'filename' => $filename]);

    }

    public function remove(Request $request){
        $url_exploded = explode(url(''), $request->url);
        if (!isset($url_exploded[1]) || empty($url_exploded[1])) {
            return response()->json(['message' => 'Unable to find path location'], 422);
        }
        Storage::delete($url_exploded[1]);
        return response()->json(['success' => 'true','url' =>$request->url]);

    }
}
