<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\User\LoginRequest;
use App\Models\User;

class LoginController extends Controller
{
    public function view(){
        $users = User::orderBy('name', 'asc')->get(); // comment on production
        return view('user.login')->withUsers($users);
    }

    public function login(LoginRequest $request){
        $validated = (object) $request->validated();

        $user = User::where('email', $validated->username)->orWhere('mobile', $validated->username)->first();

        if(empty($user)){
            return response()->json(['message' => 'Username does not match with our records'], 422);

        }

        if (!$user->verify_password($validated->password)) {
            return response()->json(['message' => 'Password does not match with our records'], 422);
        }

        session()->put('user', $user);
        $url = url('/dashboard');
        $response['redirect'] = url($url);
        return response()->json($response);
    }

    public function logout(Request $request)
    {
        session()->flush();
        return redirect()->to('/');
    }
}
