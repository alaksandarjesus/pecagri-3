<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class RequestUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = session()->get('user');
        if(empty($user)){
            $request->request->add(['user' => NULL]); 
            return $next($request);
        }
        $request->request->add(['user' => $user]);
        return $next($request);
    }
}
