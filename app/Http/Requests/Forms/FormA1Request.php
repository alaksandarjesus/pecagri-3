<?php

namespace App\Http\Requests\Forms;

use Illuminate\Foundation\Http\FormRequest;

class FormA1Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uuid' => 'nullable|exists:form_a1,uuid',
            'name'=> 'required',
            'concern'=> 'required',
            'address'=> 'required',
            'telephone'=> 'required',
            'saleAddress'=> 'required',
            'storageAddress'=> 'required',
            'fertilizerName'=> 'required',
            'certifiateSource'=> 'required',
            'fertilizerName1'=> 'required',
            'certifiateSource1'=> 'required',
            'renewal'=> 'required',
            'documents' => 'required|array',
            'relevantInfo'=> 'required',
            'application_place'=> 'required',
            'date'=> 'required|date_format:Y-m-d',
            'signature'=> 'required',
            'place'=> 'required',
            'application_date'=> 'required|date_format:Y-m-d',
            'signature1' => 'required',
        ];
    }
}
