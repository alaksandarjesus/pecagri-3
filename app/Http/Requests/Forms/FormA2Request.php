<?php

namespace App\Http\Requests\Forms;

use Illuminate\Foundation\Http\FormRequest;

class FormA2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'form_a1_uuid' => 'required',
            'concern' => 'required',
            'challanBearingNo' => 'required',
            'challanDate' => 'required|date_format:Y-m-d',
            'date' => 'required|date_format:Y-m-d',
            'license_number' => 'required',
            'date_of_issue' => 'required|date_format:Y-m-d',
            'date_of_validity' => 'required|date_format:Y-m-d',
            'signature' => 'required'
        ];
    }
}
