<?php

namespace App\Http\Requests\Forms;

use Illuminate\Foundation\Http\FormRequest;

class FormDRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uuid' => 'nullable|exists:form_d,uuid',
            'name'=> 'required',
            'address'=> 'required',
            'concern'=> 'required',
            'qualification'=> 'required',
            'new_comer'=> 'required',
            'premises'=> 'required',
            'particulars'=> 'required',
            'certificates'=> 'required',
            'duration'=> 'required',
            'quantity'=> 'required',
            'business'=> 'required',
            'quantities'=> 'required',
            'renewal'=> 'required',
            'requiring'=> 'required',
           'documents' => 'required|array',
            'applicant_name'=> 'required',
            'applicant_address'=> 'required',
            'date'=> 'required|date_format:Y-m-d',
            'application_place' => 'required',
            'signature'=> 'required',
        ];
    }
}
