<?php

namespace App\Http\Requests\Forms;

use Illuminate\Foundation\Http\FormRequest;

class FormFRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'form_d_uuid' => 'required',
            'name' => 'required',
            'certificate_number' => 'required',
            'place' => 'required',
            'signature' => 'required',
            'date_of_issue' => 'required|date_format:Y-m-d',
            'valid_upto' => 'required|date_format:Y-m-d',
            'submit_date' => 'required|date_format:Y-m-d',
        ];
    }
}

