<?php

namespace App\Http\Requests\Forms;

use Illuminate\Foundation\Http\FormRequest;

class FormIIIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'form_ii_uuid' => 'required',
            'name' => 'required',
            'license_number' => 'required',
            'address' => 'required',
            'email' => 'required',
            'otherConditions' => 'required',
            'signature' => 'required',
            'signature1' => 'required',
        ];
    }
}
