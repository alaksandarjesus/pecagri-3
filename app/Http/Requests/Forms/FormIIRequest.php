<?php

namespace App\Http\Requests\Forms;

use Illuminate\Foundation\Http\FormRequest;

class FormIIRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uuid' => 'nullable|exists:form_ii,uuid',
            'applicantphoto' => 'required',
            'name'=> 'required',
            'address'=> 'required',
            'email'=> 'required',
            'premisesAddress'=> 'required',
            'validity'=> 'required',
            'storeAddress'=> 'required',
            'pestControl'=> 'required',
            'residentArea'=> 'required',
            'foodArticle'=> 'required',
            'insecticideQualification'=> 'required',
            'courseName'=> 'required',
            'durationCourse'=> 'required',
            'certificateAward'=> 'required',
            'manufactureDetails'=> 'required',
            'commercialApplication' => 'required',
            'zonalBranchAddress'=> 'required',
            'premisesLicenceAddress'=> 'required',
            'technicalExpertise'=> 'required',
            'refApprovalNumber' => 'required',
            'itsIssueDate'=> 'required',
            'itsValidityDate'=> 'required',
            'nameRestrict'=> 'required',
            'techPersonName'=> 'required',
            'insecticideRestrict'=> 'required',
            'particularsQuantity'=> 'required',
            'detailsEquipment'=> 'required',
            'particularLicence'=> 'required',
            'otherStateApplicant'=> 'required',
            'stateLicenceNumber'=> 'required',
            'grantDate'=> 'required',
            'ApplicationFee'=> 'required',
            'documents'=> 'required|array',
            'relevantInfo'=> 'required',
            'signature'=> 'required',
            'decName'=> 'required',
            'sonOfname'=> 'required',
            'capacityAs'=> 'required',
            'virtueOf'=> 'required',
            'application_place'=> 'required',
            'date'=> 'required',
            'signature1'=> 'required',
        ];
    }
}
