<?php

namespace App\Models\Forms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormA1 extends Model
{
    use SoftDeletes;

    protected $table = 'form_a1';

    protected $fillable = [];

    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'];
    
    protected $casts = ['documents' => 'object', 'edit_required' => 'boolean'];

    public function forma2(){
        return $this->hasOne('App\Models\Forms\FormA2', 'id', 'form_a2_id');
    }



    public function getCanViewAttribute(){
        $user = request()->user;
        if(empty($user)){
            return false;
        }
        if($this->created_by === $user->id){
            return true;
        }
        if($user->can_process_form_a1){
            return true;
        }
        return false;
    }

    public function getCanEditAttribute(){
        $user = request()->user;
        if(empty($user)){
            return false;
        }
        if(!$this->edit_required){
            return false;
        }

        return $this->created_by  === $user->id;
    }



   
    public function owner(){
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function process(){
        return $this->hasOne('App\Models\Forms\FormA1Process', 'form_a1_id', 'id')->orderBy('id', 'desc');
    }

    public function getCanProcessAttribute(){
        $user = request()->user;
        if(empty($user)){
            return false;
        }
        if(!$user->can_process_form_a1){
            return false;
        }
        if($this->can_edit){
            return false;
        }
        if($this->form_a2_id){
            return false;
        }
        if($user->is_ao && !$this->process->verified_by_ao){
            return true;
        }

        if($user->is_dda && !$this->process->verified_by_dda && $this->process->verified_by_ao){
            return true;
        }
        if($user->is_jda && !$this->process->verified_by_jda && $this->process->verified_by_dda){
            return true;
        }
        if($user->is_adaa && !$this->process->verified_by_adaa && $this->process->verified_by_jda){
            return true;
        }
     
        return false;
    }

    public function getGenerateFormA2Attribute(){

        $user = request()->user;
        if(empty($user)){
            return false;
        }

        if($this->form_a2_id){
            return false;
        }

        if($user->is_adaa && $this->process->verified_by_adaa){
            return true;
        }
     
        return false;

    }
    

}