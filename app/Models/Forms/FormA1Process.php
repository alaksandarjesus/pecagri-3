<?php

namespace App\Models\Forms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormA1Process extends Model
{
    use SoftDeletes;

    protected $table = 'form_a1_process';

    protected $fillable = [];

    protected $hidden = ['id', 'deleted_at'];
    
    protected $casts = [];
}
