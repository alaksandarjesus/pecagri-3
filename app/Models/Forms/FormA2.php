<?php

namespace App\Models\Forms;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class FormA2 extends Model
{
    use SoftDeletes;

    protected $table = 'form_a2';

    protected $fillable = [];

    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'];
    
    protected $casts = [];
}

