<?php

namespace App\Models\Forms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormD extends Model
{
    use SoftDeletes;

    protected $table = 'form_d';

    protected $fillable = [];

    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'];
    
    protected $casts = ['documents' => 'object', 'edit_required' => 'boolean'];

    public function formf(){
        return $this->hasOne('App\Models\Forms\FormF', 'id', 'form_f_id');
    }

    public function getCanViewAttribute(){
        $user = request()->user;
        if(empty($user)){
            return false;
        }
        if($this->created_by === $user->id){
            return true;
        }
        if($user->can_process_form_d){
            return true;
        }
        return false;
    }

    public function getCanEditAttribute(){
        $user = request()->user;
        if(empty($user)){
            return false;
        }
        if(!$this->edit_required){
            return false;
        }

        return $this->created_by  === $user->id;
    }



   
    public function owner(){
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function process(){
        return $this->hasOne('App\Models\Forms\FormDProcess', 'form_d_id', 'id')->orderBy('id', 'desc');
    }

    public function getCanProcessAttribute(){
        $user = request()->user;
        if(empty($user)){
            return false;
        }
        if(!$user->can_process_form_d){
            return false;
        }
        if($this->can_edit){
            return false;
        }
        if($this->form_f_id){
            return false;
        }
        if($user->is_ao && !$this->process->verified_by_ao){
            return true;
        }

        if($user->is_dda && !$this->process->verified_by_dda && $this->process->verified_by_ao){
            return true;
        }
        if($user->is_jda && !$this->process->verified_by_jda && $this->process->verified_by_dda){
            return true;
        }
        if($user->is_ada && !$this->process->verified_by_ada && $this->process->verified_by_jda){
            return true;
        }
     
        if($user->is_director && !$this->process->verified_by_director && $this->process->verified_by_ada){
            return true;
        }
     
        return false;
    }

    public function getGenerateFormFAttribute(){

        $user = request()->user;
        if(empty($user)){
            return false;
        }

        if($this->form_f_id){
            return false;
        }

        if($user->is_director && $this->process->verified_by_director){
            return true;
        }
     
        return false;

    }
    
}
