<?php

namespace App\Models\Forms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormII extends Model
{
    protected $table = 'form_ii';

    protected $fillable = [];

    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'];
    
    protected $casts = ['documents' => 'object', 'edit_required' => 'boolean'];

    public function formiii(){
        return $this->hasOne('App\Models\Forms\FormIII', 'id', 'form_iii_id');
    }

    public function getCanViewAttribute(){
        $user = request()->user;
        if(empty($user)){
            return false;
        }
        if($this->created_by === $user->id){
            return true;
        }
        if($user->can_process_form_ii){
            return true;
        }
        return false;
    }

    public function getCanEditAttribute(){
        $user = request()->user;
        if(empty($user)){
            return false;
        }
        if(!$this->edit_required){
            return false;
        }

        return $this->created_by  === $user->id;
    }

    public function owner(){
        return $this->hasOne('App\Models\User', 'id', 'created_by');
    }

    public function process(){
        return $this->hasOne('App\Models\Forms\FormIIProcess', 'form_ii_id', 'id')->orderBy('id', 'desc');
    }

    public function getCanProcessAttribute(){
        $user = request()->user;
        if(empty($user)){
            return false;
        }
        if(!$user->can_process_form_ii){
            return false;
        }
        if($this->can_edit){
            return false;
        }
        if($this->form_iii_id){
            return false;
        }
        if($user->is_aoqc && !$this->process->verified_by_aoqc){
            return true;
        }

        if($user->is_dda && !$this->process->verified_by_dda && $this->process->verified_by_aoqc){
            return true;
        }
        if($user->is_jda && !$this->process->verified_by_jda && $this->process->verified_by_dda){
            return true;
        }
        if($user->is_ada && !$this->process->verified_by_ada && $this->process->verified_by_jda){
            return true;
        }
     
        if($user->is_director && !$this->process->verified_by_director && $this->process->verified_by_ada){
            return true;
        }
     
        return false;
    }

    public function getGenerateFormIIIAttribute(){

        $user = request()->user;
        if(empty($user)){
            return false;
        }

        if($this->form_iii_id){
            return false;
        }

        if($user->is_director && $this->process->verified_by_director){
            return true;
        }
     
        return false;

    }


}