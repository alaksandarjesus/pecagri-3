<?php

namespace App\Models\Forms;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class FormF extends Model
{
    use SoftDeletes;

    protected $table = 'form_iii';

    protected $fillable = [];

    protected $hidden = ['id', 'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by', 'deleted_by'];
    
    protected $casts = [];
}

