<?php

namespace App\Forms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormIIProcess extends Model
{
    use SoftDeletes;

    protected $table = 'form_ii_process';

    protected $fillable = [];

    protected $hidden = ['id', 'deleted_at'];
    
    protected $casts = [];
}
