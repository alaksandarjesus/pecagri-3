<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

class User extends Model
{
    use SoftDeletes;

    protected $table = 'users';

    protected $fillable = [];

    protected $hidden = ['id', 'created_by', 'updated_by', 'deleted_by', 'created_at', 'updated_at',
        'deleted_at', 'role_id'
        , 'verified_at', 'password', 'code', 'remember_token', 'token_created_at', 'blocked'];

    protected $casts = [];

    public function setPasswordAttribute($value)
    {

        return $this->attributes['password'] = Hash::make($value);

    }

    public function role()
    {
        return $this->hasOne('App\Models\Role', 'id', 'role_id');
    }

    public function verify_password($password)
    {
        return Hash::check($password, $this->password);
    }

    public function getIsAoAttribute()
    {
        return $this->role->name === 'Agriculture Officer';
    }

    public function getIsAoqcAttribute()
    {
        return $this->role->name === 'Agriculture Officer QCI';
    }

    public function getIsAdminAttribute()
    {
        return $this->role->name === 'Administrator';
    }

    public function getIsDdaAttribute()
    {
        return $this->role->name === 'Deputy Director of Agriculture';
    }
    public function getIsJdaAttribute()
    {
        return $this->role->name === 'Joint Director of Agriculture';
    }

    public function getIsAdaAttribute()
    {
        return $this->role->name === 'Additional Director of Agriculture';
    }

    public function getIsAdaaAttribute()
    {
        return $this->role->name === 'Additional Director of Agriculture Authority';
    }

    public function getIsDirectorAttribute()
    {
        return $this->role->name === 'Director';
    }

    public function getCanProcessFormDAttribute()
    {
        $roles = [
            'Agriculture Officer',
            'Deputy Director of Agriculture',
            'Joint Director of Agriculture',
            'Additional Director of Agriculture',
            'Director'];
        $user = request()->user;
        if (empty($user)) {
            return false;
        }
        return in_array($user->role->name, $roles) === true;
    }

    public function getCanApplyFormDAttribute()
    {
        $roles = [
            'Bio Fertilizer',
            'Organic Fertilizer', 'Physical Mixture Fertilizer', 'Granulated Mixture Fertilizer'];
        $user = request()->user;
        if (empty($user)) {
            return false;
        }
        return in_array($user->role->name, $roles) === true;
    }

    public function getCanProcessFormIIAttribute()
    {
        $roles = [
            'Agriculture Officer QCI',
            'Deputy Director of Agriculture',
            'Joint Director of Agriculture',
            'Additional Director of Agriculture',
            'Director'];
        $user = request()->user;
        if (empty($user)) {
            return false;
        }
        return in_array($user->role->name, $roles) === true;
    }

    public function getCanApplyFormIIAttribute()
    {
        $roles = [
            'Manufacturer Insecticides',
            'Distributor Insecticides',
            'Pest Control Operations'];
        $user = request()->user;
        if (empty($user)) {
            return false;
        }
        return in_array($user->role->name, $roles) === true;
    }

    public function getCanProcessFormA1Attribute()
    {
        $roles = [
            'Agriculture Officer',
            'Deputy Director of Agriculture',
            'Joint Director of Agriculture',
            'Additional Director of Agriculture',
            'Additional Director of Agriculture Authority'];
        $user = request()->user;
        if (empty($user)) {
            return false;
        }
        return in_array($user->role->name, $roles) === true;
    }

    public function getCanApplyFormA1Attribute()
    {
        $roles = [
            'Wholesale Dealer',
            'Retail Dealer', 'Manufacturer', 'Importer', 'Pool Handling Agency'];
        $user = request()->user;
        if (empty($user)) {
            return false;
        }
        return in_array($user->role->name, $roles) === true;
    }

    

}
