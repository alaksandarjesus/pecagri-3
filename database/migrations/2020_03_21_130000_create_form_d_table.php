<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormDTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_d', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('form_f_id')->nullable();
            $table->string('name', 150);
            $table->string('address', 150);
            $table->string('concern', 150);
            $table->string('qualification', 150);
            $table->string('new_comer', 150);
            $table->string('premises', 150);
            $table->string('particulars', 150);
            $table->string('certificates', 150);
            $table->string('duration', 150);
            $table->string('quantity', 150);
            $table->string('business', 150);
            $table->string('quantities', 150);
            $table->string('renewal', 150);
            $table->string('requiring', 150);
            $table->text('documents');
            $table->string('applicant_name', 150);
            $table->string('applicant_address', 150);
            $table->string('application_place', 150);
            $table->date('date', 150);
            $table->text('signature');
            $table->boolean('edit_required')->default(0)->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_d');
    }
}
