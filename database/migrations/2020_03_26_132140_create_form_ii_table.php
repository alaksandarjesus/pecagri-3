<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormIiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_ii', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('form_iii_id')->nullable();
            $table->text('applicantphoto');
            $table->string('name', 150);
            $table->string('address', 150);
            $table->string('email', 150);
            $table->string('premisesAddress', 150);
            $table->string('validity', 150);
            $table->string('storeAddress', 150);
            $table->string('pestControl', 150);
            $table->string('residentArea', 150);
            $table->string('foodArticle', 150);
            $table->string('insecticideQualification', 150);
            $table->string('courseName', 150);
            $table->string('durationCourse', 150);
            $table->string('certificateAward', 150);
            $table->string('manufactureDetails', 150);
            $table->string('commercialApplication', 150);
            $table->string('zonalBranchAddress', 150);
            $table->string('premisesLicenceAddress', 150);
            $table->string('technicalExpertise', 150);
            $table->string('refApprovalNumber', 150);
            $table->string('itsIssueDate', 150);
            $table->string('itsValidityDate', 150);
            $table->string('nameRestrict', 150);
            $table->string('techPersonName', 150);
            $table->string('insecticideRestrict', 150);
            $table->string('particularsQuantity', 150);
            $table->string('detailsEquipment', 150);
            $table->string('particularLicence', 150);
            $table->string('otherStateApplicant', 150);
            $table->string('stateLicenceNumber', 150);
            $table->string('grantDate', 150);
            $table->string('ApplicationFee', 150);
            $table->text('documents');
            $table->string('relevantInfo', 150);
            $table->text('signature');
            $table->string('decName', 150);
            $table->string('sonOfname', 150);
            $table->string('capacityAs', 150);
            $table->string('virtueOf', 150);
            $table->string('application_place', 150);
            $table->string('date', 150);
            $table->text('signature1');
            $table->boolean('edit_required')->default(0)->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_ii');
    }
}
