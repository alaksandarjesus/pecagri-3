<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormIiProcessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_ii_process', function (Blueprint $table) {
            $table->id();
            $table->integer('form_ii_id');
            $table->date('verified_by_aoqc')->nullable();
            $table->text('rejected_by_aoqc')->nullable();
            $table->date('verified_by_dda')->nullable();
            $table->text('rejected_by_dda')->nullable();
            $table->date('verified_by_jda')->nullable();
            $table->text('rejected_by_jda')->nullable();
            $table->date('verified_by_ada')->nullable();
            $table->text('rejected_by_ada')->nullable();
            $table->date('verified_by_director')->nullable();
            $table->text('rejected_by_director')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_ii_process');
    }
}
