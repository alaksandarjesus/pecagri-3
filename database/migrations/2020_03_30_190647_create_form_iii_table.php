<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormIiiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_iii', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('form_ii_id')->unique();
            $table->string('name', 150);
            $table->text('address', 150);
            $table->text('email', 150);
            $table->string('license_number', 150);
            $table->string('otherConditions', 150);
            $table->text('signature', 150);
            $table->text('signature1', 150);
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_iii');
    }
}
