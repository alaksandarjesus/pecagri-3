<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormA1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_a1', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('form_a2_id')->nullable();
            $table->string('name', 150);
            $table->string('concern', 150);
            $table->string('address', 150);
            $table->string('telephone', 150);
            $table->string('saleAddress', 150);
            $table->string('storageAddress', 150);
            $table->string('fertilizerName', 150);
            $table->string('certifiateSource', 150);
            $table->string('fertilizerName1', 150);
            $table->string('certifiateSource1', 150);
            $table->string('renewal', 150);
            $table->text('documents');
            $table->string('relevantInfo', 150);
            $table->string('application_place', 150); 
            $table->date('date');
            $table->text('signature');
            $table->text('place');
            $table->date('application_date');
            $table->text('signature1');
            $table->boolean('edit_required')->default(0)->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_a1');
    }
}
