<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormA2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_a2', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->integer('form_a1_id')->unique();
            $table->string('concern', 150);
            $table->text('challanBearingNo', 150);
            $table->date('challanDate', 150);
            $table->date('date', 150);
            $table->text('license_number', 150);
            $table->date('date_of_issue', 150);
            $table->date('date_of_validity', 150);
            $table->text('signature', 150);
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_a2');
    }
}
