<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use Illuminate\Support\Str;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['Administrator', 'Agriculture Officer', 'Agriculture Officer QCI', 'Deputy Director of Agriculture',
         'Joint Director of Agriculture', 'Additional Director of Agriculture', 'Additional Director of Agriculture Authority', 'Director', 
         'Bio Fertilizer',
        'Organic Fertilizer', 'Physical Mixture Fertilizer', 'Granulated Mixture Fertilizer', 'Wholesale Dealer', 'Retail Dealer', 'Manufacturer',
    'Importer', 'Pool Handling Agency', 'Manufacturer Insecticides', 'Distributor Insecticides', 'Pest Control Operations'];

        foreach($roles as $role){
            $args = [
                'name' => $role,
                'uuid' => Str::uuid(),
                'created_by' => 1
            ];
            Role::create($args);
        }
    }
}
