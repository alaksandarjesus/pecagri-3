<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = ['Administrator', 'Agriculture Officer', 'Agriculture Officer QCI', 'Deputy Director of Agriculture',
        'Joint Director of Agriculture', 'Additional Director of Agriculture', 'Additional Director of Agriculture Authority', 'Director', 'Bio Fertilizer',
       'Organic Fertilizer', 'Physical Mixture Fertilizer', 'Granulated Mixture Fertilizer', 'Wholesale Dealer', 'Retail Dealer', 'Manufacturer',
   'Importer', 'Dool Handling Agency', 'Manufacturer Insecticides', 'Distributor Insecticides', 'Pest Control Operations'];

        foreach($users as $index => $user){
            $args = [
                'name' => $user,
                'uuid' => Str::uuid(),
                'password' => 'Password@123',
                'role_id' => $index+1,
                'email' =>Str::kebab($user).'@example.com',
                'mobile' => $index<9?'100000000'.($index+1):'1000000'.($index+1),
                'created_by' => 1
            ];

            User::create($args);

        }

    }
}
