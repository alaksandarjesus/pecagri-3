/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/form-f.js":
/*!********************************!*\
  !*** ./resources/js/form-f.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./form-f/app */ "./resources/js/form-f/app.js");

/***/ }),

/***/ "./resources/js/form-f/FormfFormController.js":
/*!****************************************************!*\
  !*** ./resources/js/form-f/FormfFormController.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function () {
  angular.module('formf').controller('FormfFormController', FormfFormController);
})();

function FormfFormController($scope, $timeout, Upload) {
  $timeout(function () {
    onInit();
  });
  $scope.formd = {};
  $scope.params = {};
  $scope.form = {
    form_d_uuid: '',
    name: '',
    certificate_number: '',
    date_of_issue: '',
    valid_upto: '',
    submit_date: '',
    signature: '',
    place: ''
  };

  $scope.uploadSignatureFile = function (file) {
    Upload.upload({
      url: window.APP.API + '/upload/formd',
      data: {
        file: file,
        filename: 'formf-signature'
      }
    }).then(function (resp) {
      $scope.form.signature = resp.data.url;
    }, function (resp) {
      alert("error uploading file");
      return;
    });
  };

  $scope.onRemoveSignatureFile = function (file) {
    axios.post('file-remove', {
      url: $scope.form.signature
    }).then(function (response) {
      $scope.form.signature = null;
    });
  };

  $scope.formDisabled = function () {
    if (!$scope.form.signature) {
      return true;
    }

    return false;
  };

  $scope.onSubmit = function (event) {
    event.preventDefault();
    event.stopPropagation();

    var data = _.cloneDeep($scope.form);

    data.submit_date = moment(data.submit_date, 'DD-MM-YYYY').format('YYYY-MM-DD');
    data.date_of_issue = moment(data.date_of_issue, 'DD-MM-YYYY').format('YYYY-MM-DD');
    data.valid_upto = moment(data.valid_upto, 'DD-MM-YYYY').format('YYYY-MM-DD');
    axios.post('form-f', data).then(function () {});
  };

  function onInit() {
    $scope.form.submit_date = moment().format('DD-MM-YYYY');
    $scope.form.date_of_issue = moment().format('DD-MM-YYYY');
    $scope.form.valid_upto = moment().add(5, 'years').format('DD-MM-YYYY');
    $scope.form.name = $scope.formd.name;
    $scope.form.certificate_number = $scope.params.certificate_number;
    $scope.form.form_d_uuid = $scope.formd.uuid;
  }
}

FormfFormController.$inject = ['$scope', '$timeout', 'Upload'];

/***/ }),

/***/ "./resources/js/form-f/app.js":
/*!************************************!*\
  !*** ./resources/js/form-f/app.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function () {
  angular.module('formf', ['sharedModule']);
})();

__webpack_require__(/*! ./FormfFormController */ "./resources/js/form-f/FormfFormController.js");

/***/ }),

/***/ 5:
/*!**************************************!*\
  !*** multi ./resources/js/form-f.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\pecagri-3\resources\js\form-f.js */"./resources/js/form-f.js");


/***/ })

/******/ });