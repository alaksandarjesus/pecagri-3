require('angular');
require('angular-material');
require('angular-animate');
require('angular-aria');
require('angular-messages');
require('ng-file-upload/dist/ng-file-upload-all.min.js')
require('./shared/app');

require('./bootstrap');
require('./axios');
