(function () {
    angular.module('forma1').controller('Forma1FormController', Forma1FormController);
})();

function Forma1FormController($scope, $timeout, Upload) {
    $timeout(()=>{
        onInit();
    })
    $scope.form = {
        uuid:"",
        name: 'as',
        concern: '',
        address: 'as',
        telephone: '',
        saleAddress: '',
        storageAddress: '',
        fertilizerName: '',
        certifiateSource: '',
        fertilizerName1: '',
        certifiateSource1: '',
        renewal: '',
        documents: [],
        relevantInfo: 'as',
        application_place: 'as',
        date: 'as',
        signature: '',
        place: '',
        application_date: '',
        signature1: '',
    }

    $scope.uploadMultipleFiles = function (files) {
        if (files && files.length) {
            _.forEach(files, (file) => {
                Upload.upload({
                    url: window.APP.API + '/upload/forma1',
                    data: { file: file }
                }).then(function (resp) {
                    $scope.form.documents.push(resp.data)
                }, function (resp) {
                    alert("error uploading file");
                    return;
                })
            });
        }
    }

    $scope.uploadSignatureFile = function (file) {
        Upload.upload({
            url: window.APP.API + '/upload/forma1',
            data: { file: file, filename: 'signature' }
        }).then(function (resp) {
            $scope.form.signature = resp.data.url
        }, function (resp) {
            alert("error uploading file");
            return;
        })
    }

    $scope.uploadSignature1File = function (file) {
        Upload.upload({
            url: window.APP.API + '/upload/forma1',
            data: { file: file, filename: 'signature1' }
        }).then(function (resp) {
            $scope.form.signature1 = resp.data.url
        }, function (resp) {
            alert("error uploading file");
            return;
        })
    }

    $scope.onRemoveFile = function (document) {
        axios.post('file-remove', { url: document.url })
            .then(function (response) {
                _.remove($scope.form.documents, (document) => document.url === response.url);
            })
    }

    $scope.onRemoveSignatureFile = function (file) {
        axios.post('file-remove', { url: $scope.form.signature })
            .then(function (response) {
                $scope.form.signature = null;
            })
    }

    $scope.onRemoveSignature1File = function (file) {
        axios.post('file-remove', { url: $scope.form.signature1 })
            .then(function (response) {
                $scope.form.signature1 = null;
            })
    }

    function onInit(){
        $scope.form.date = moment().format('DD-MM-YYYY');
        $scope.form.application_date = moment().format('DD-MM-YYYY');
    }
    $scope.formDisabled = function(){
        if(!$scope.form.documents || !$scope.form.documents.length){
            return true;
        }
        if(!$scope.form.signature){
            return true;
        }

        if(!$scope.form.signature1){
            return true;
        }
        return false;
    }
    $scope.onSubmit = function(event){
        event.preventDefault();
        event.stopPropagation();
        const data = _.cloneDeep($scope.form);
        data.date = moment(data.date, 'DD-MM-YYYY').format('YYYY-MM-DD');
        data.application_date = moment(data.application_date, 'DD-MM-YYYY').format('YYYY-MM-DD');
        axios.post('form-a1', data).then(function(){

        });
    }


}

Forma1FormController.$inject = ['$scope', '$timeout','Upload'];