(function () {
    angular.module('forma2').controller('Forma2FormController', Forma2FormController);
})();

function Forma2FormController($scope, $timeout, Upload) {
    $timeout(()=>{
        onInit();
    })
    $scope.forma1 = {};
    $scope.params = {};
    $scope.form = {
        form_a1_uuid:'',
        concern:'',
        challanBearingNo:'',
        challanDate: '',
        date :'',
        license_number:'',
        date_of_issue:'',
        date_of_validity: '',
        signature: ''
    }

    $scope.uploadSignatureFile = function (file) {
        Upload.upload({
            url: window.APP.API + '/upload/forma1',
            data: { file: file, filename: 'forma2-signature' }
        }).then(function (resp) {
            $scope.form.signature = resp.data.url
        }, function (resp) {
            alert("error uploading file");
            return;
        })
    }

    $scope.onRemoveSignatureFile = function (file) {
        axios.post('file-remove', { url: $scope.form.signature })
            .then(function (response) {
                $scope.form.signature = null;
            })
    }

    $scope.formDisabled = function(){
        if(!$scope.form.signature){
            return true;
        }
        return false;
    }


    $scope.onSubmit = function(event){
        event.preventDefault();
        event.stopPropagation();
        const data = _.cloneDeep($scope.form);
        data.challanDate = moment(data.challanDate, 'DD-MM-YYYY').format('YYYY-MM-DD');
        data.date = moment(data.date, 'DD-MM-YYYY').format('YYYY-MM-DD');
        data.date_of_issue = moment(data.date_of_issue, 'DD-MM-YYYY').format('YYYY-MM-DD');
        data.date_of_validity = moment(data.date_of_validity, 'DD-MM-YYYY').format('YYYY-MM-DD');
        axios.post('form-f', data).then(function(){

        });
    }


    function onInit(){
        $scope.form.challanDate = moment().format('DD-MM-YYYY');
        $scope.form.date = moment().format('DD-MM-YYYY');
        $scope.form.date_of_issue = moment().format('DD-MM-YYYY');
        $scope.form.date_of_validity = moment().add(5, 'years').format('DD-MM-YYYY');
        $scope.form.concern = $scope.forma1.concern;
        $scope.form.license_number = $scope.params.license_number;
        $scope.form.form_a1_uuid = $scope.forma1.uuid;
    }

}


Forma2FormController.$inject = ['$scope', '$timeout', 'Upload'];

