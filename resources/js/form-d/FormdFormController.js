(function () {
    angular.module('formd').controller('FormdFormController', FormdFormController);
})();

function FormdFormController($scope, $timeout, Upload) {
    $timeout(()=>{
        onInit();
    })
    $scope.form = {
        uuid:"",
        name: 'as',
        address: 'as',
        concern: '',
        qualification: 'No',
        new_comer: 'No',
        premises: 'as',
        particulars: 'as',
        certificates: 'as',
        duration: 'as',
        quantity: '',
        business: 'as',
        quantities: 'as',
        renewal: 'as',
        requiring: 'as',
        documents: [],
        applicant_name: 'as',
        applicant_address: 'as',
        application_place: 'as',
        date: 'as',
        signature: ''
    }

    $scope.uploadMultipleFiles = function (files) {
        if (files && files.length) {
            _.forEach(files, (file) => {
                Upload.upload({
                    url: window.APP.API + '/upload/formd',
                    data: { file: file }
                }).then(function (resp) {
                    $scope.form.documents.push(resp.data)
                }, function (resp) {
                    alert("error uploading file");
                    return;
                })
            });
        }
    }

    $scope.uploadSignatureFile = function (file) {
        Upload.upload({
            url: window.APP.API + '/upload/formd',
            data: { file: file, filename: 'signature' }
        }).then(function (resp) {
            $scope.form.signature = resp.data.url
        }, function (resp) {
            alert("error uploading file");
            return;
        })
    }

    $scope.onRemoveFile = function (document) {
        axios.post('file-remove', { url: document.url })
            .then(function (response) {
                _.remove($scope.form.documents, (document) => document.url === response.url);
            })
    }

    $scope.onRemoveSignatureFile = function (file) {
        axios.post('file-remove', { url: $scope.form.signature })
            .then(function (response) {
                $scope.form.signature = null;
            })
    }

    function onInit(){
        $scope.form.date = moment().format('DD-MM-YYYY');
    }
    $scope.formDisabled = function(){
        if(!$scope.form.documents || !$scope.form.documents.length){
            return true;
        }
        if(!$scope.form.signature){
            return true;
        }
        return false;
    }
    $scope.onSubmit = function(event){
        event.preventDefault();
        event.stopPropagation();
        const data = _.cloneDeep($scope.form);
        data.date = moment(data.date, 'DD-MM-YYYY').format('YYYY-MM-DD');
        axios.post('form-d', data).then(function(){

        });
    }

}


FormdFormController.$inject = ['$scope', '$timeout','Upload'];