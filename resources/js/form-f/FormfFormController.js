(function () {
    angular.module('formf').controller('FormfFormController', FormfFormController);
})();

function FormfFormController($scope, $timeout, Upload) {
    $timeout(()=>{
        onInit();
    })
    $scope.formd = {};
    $scope.params = {};
    $scope.form = {
        form_d_uuid:'',
        name:'',
        certificate_number:'',
        date_of_issue:'',
        valid_upto: '',
        submit_date :'',
        signature:'',
        place:''
    }

    $scope.uploadSignatureFile = function (file) {
        Upload.upload({
            url: window.APP.API + '/upload/formd',
            data: { file: file, filename: 'formf-signature' }
        }).then(function (resp) {
            $scope.form.signature = resp.data.url
        }, function (resp) {
            alert("error uploading file");
            return;
        })
    }

    $scope.onRemoveSignatureFile = function (file) {
        axios.post('file-remove', { url: $scope.form.signature })
            .then(function (response) {
                $scope.form.signature = null;
            })
    }

    $scope.formDisabled = function(){
        if(!$scope.form.signature){
            return true;
        }
        return false;
    }


    $scope.onSubmit = function(event){
        event.preventDefault();
        event.stopPropagation();
        const data = _.cloneDeep($scope.form);
        data.submit_date = moment(data.submit_date, 'DD-MM-YYYY').format('YYYY-MM-DD');
        data.date_of_issue = moment(data.date_of_issue, 'DD-MM-YYYY').format('YYYY-MM-DD');
        data.valid_upto = moment(data.valid_upto, 'DD-MM-YYYY').format('YYYY-MM-DD');
        axios.post('form-f', data).then(function(){

        });
    }


    function onInit(){
        $scope.form.submit_date = moment().format('DD-MM-YYYY');
        $scope.form.date_of_issue = moment().format('DD-MM-YYYY');
        $scope.form.valid_upto = moment().add(3, 'years').format('DD-MM-YYYY');
        $scope.form.name = $scope.formd.name;
        $scope.form.certificate_number = $scope.params.certificate_number;
        $scope.form.form_d_uuid = $scope.formd.uuid;
    }

}


FormfFormController.$inject = ['$scope', '$timeout', 'Upload'];

