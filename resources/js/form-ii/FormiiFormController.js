(function () {
    angular.module('formii').controller('FormiiFormController', FormiiFormController);
})();

function FormiiFormController($scope, $timeout, Upload) {
    const insecticide_name_reg_no_row = {
        insecticide_name: '',
        registration_number: ''
    }
    const name_designation_qualification_experience_row = {
        name: '',
        desingation:'',
        qualification: '',
        experience: ''
    }
    const license_to_sell_stock_insecticide_row = {
        particulars: '',
        name: '',
        certificate_number: '',
        date: ''
    }
    $timeout(()=>{
        onInit();
    })
    $scope.form = {
        uuid:"",
        applicant_photo: '',
        name: 'name',
        address: 'address',
        email: 'email@example.com',
        premises_address: 'premises address',
        insecticide_name_reg_no: [insecticide_name_reg_no_row],
        name_designation_qualification_experience: [name_designation_qualification_experience_row],
        license_to_sell_stock_insecticide: [license_to_sell_stock_insecticide_row],
        validity: 'validity',
        store_address: 'as',
        pestControl: 'as',
        residentArea: 'No',
        foodArticle: 'No',
        insecticideQualification: 'No',
        courseName: 'as',
        durationCourse: 'as',
        certificateAward: 'No',
        manufactureDetails: 'No',
        commercialApplication: 'No',
        zonalBranchAddress: 'as',
        premisesLicenceAddress: 'as',
        technicalExpertise: 'No',
        refApprovalNumber: '',
        itsIssueDate: 'as',
        itsValidityDate: 'as',
        nameRestrict: 'as',
        techPersonName: 'as',
        insecticideRestrict: 'No',
        particularsQuantity: 'as',
        detailsEquipment: 'as',
        particularLicence: 'as',
        otherStateApplicant: 'No',
        stateLicenceNumber: 'as',
        grantDate: 'as',
        ApplicationFee: 'as',
        documents: [],
        relevantInfo: 'as',
        signature: '',
        decName: 'decname',
        sonOfname: 'as',
        capacityAs: 'as',
        virtueOf: 'as',
        application_place: 'Pondicherry',
        date: 'as',
        signature1: '',
        guardianAs: ''
    }

    $scope.uploadMultipleFiles = function (files) {
        if (files && files.length) {
            _.forEach(files, (file) => {
                Upload.upload({
                    url: window.APP.API + '/upload/formii',
                    data: { file: file }
                }).then(function (resp) {
                    $scope.form.documents.push(resp.data)
                }, function (resp) {
                    alert("error uploading file");
                    return;
                })
            });
        }
    }

    $scope.uploadApplicantPhotoFile = function (file) {
        Upload.upload({
            url: window.APP.API + '/upload/formii',
            data: { file: file, filename: 'applicant_photo' }
        }).then(function (resp) {
            $scope.form.applicant_photo = resp.data.url
        }, function (resp) {
            alert("error uploading file");
            return;
        })
    }

    $scope.uploadSignatureFile = function (file) {
        Upload.upload({
            url: window.APP.API + '/upload/formii',
            data: { file: file, filename: 'signature' }
        }).then(function (resp) {
            $scope.form.signature = resp.data.url
        }, function (resp) {
            alert("error uploading file");
            return;
        })
    }

    $scope.uploadSignature1File = function (file) {
        Upload.upload({
            url: window.APP.API + '/upload/formii',
            data: { file: file, filename: 'signature1' }
        }).then(function (resp) {
            $scope.form.signature1 = resp.data.url
        }, function (resp) {
            alert("error uploading file");
            return;
        })
    }

    $scope.onRemoveFile = function (document) {
        axios.post('file-remove', { url: document.url })
            .then(function (response) {
                _.remove($scope.form.documents, (document) => document.url === response.url);
            })
    }

    $scope.onRemoveApplicantPhotoFile = function (file) {
        axios.post('file-remove', { url: $scope.form.applicant_photo })
            .then(function (response) {
                $scope.form.applicant_photo = null;
            })
    }
    
    $scope.onRemoveSignatureFile = function (file) {
        axios.post('file-remove', { url: $scope.form.signature })
            .then(function (response) {
                $scope.form.signature = null;
            })
    }

    $scope.onRemoveSignature1File = function (file) {
        axios.post('file-remove', { url: $scope.form.signature1 })
            .then(function (response) {
                $scope.form.signature1 = null;
            })
    }

    function onInit(){
        $scope.form.date = moment().format('DD-MM-YYYY');
    }
    $scope.formDisabled = function(){
        if(!$scope.form.documents || !$scope.form.documents.length){
            return true;
        }
        if(!$scope.form.signature || !$scope.form.signature1){
            return true;
        }
        if(!$scope.form.applicant_photo){
            return true;
        }
        return false;
    }

    $scope.onSubmit = function(event){
        event.preventDefault();
        event.stopPropagation();
        const data = _.cloneDeep($scope.form);
        data.date = moment(data.date, 'DD-MM-YYYY').format('YYYY-MM-DD');
        axios.post('form-ii', data).then(function(){

        });
    }

    $scope.addInsecticideNameRegNoRow = function(event){
        event.preventDefault();
        event.stopPropagation();
        this.form.insecticide_name_reg_no.push(_.cloneDeep(insecticide_name_reg_no_row));
    }

    $scope.removeInsecticideNameRegNoRow = function(index){
        this.form.insecticide_name_reg_no.splice(index, 1);
    }

    $scope.addNameDesignationQualificationExperience = function(event){
        event.preventDefault();
        event.stopPropagation();
        this.form.name_designation_qualification_experience.push(_.cloneDeep(name_designation_qualification_experience_row));
    }

    $scope.removeNameDesignationQualificationExperience = function(index){
        this.form.name_designation_qualification_experience.splice(index, 1);
    }

    $scope.addLicenseToSellStockInsecticide = function(event){
        event.preventDefault();
        event.stopPropagation();
        this.form.license_to_sell_stock_insecticide.push(_.cloneDeep(license_to_sell_stock_insecticide_row));
    }

    $scope.removeLicenseToSellStockInsecticide  = function(index){
        this.form.license_to_sell_stock_insecticide.splice(index, 1);
    }

    

}

FormiiFormController.$inject = ['$scope', '$timeout','Upload'];