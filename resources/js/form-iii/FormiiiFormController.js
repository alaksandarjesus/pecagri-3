(function () {
    angular.module('formiii').controller('FormiiiFormController', FormiiiFormController);
})();

function FormiiiFormController($scope, $timeout, Upload) {
    $timeout(()=>{
        onInit();
    })
    $scope.formii = {};
    $scope.params = {};
    $scope.form = {
        form_ii_uuid:'',
        name:'',
        license_number:'',
        address:'',
        email: '',
        otherConditions :'',
        signature:'',
        signature1:''
    }

    $scope.uploadSignatureFile = function (file) {
        Upload.upload({
            url: window.APP.API + '/upload/formiii',
            data: { file: file, filename: 'formiii-signature' }
        }).then(function (resp) {
            $scope.form.signature = resp.data.url
        }, function (resp) {
            alert("error uploading file");
            return;
        })
    }

    $scope.onRemoveSignatureFile = function (file) {
        axios.post('file-remove', { url: $scope.form.signature })
            .then(function (response) {
                $scope.form.signature = null;
            })
    }

    $scope.uploadSignature1File = function (file) {
        Upload.upload({
            url: window.APP.API + '/upload/formiii',
            data: { file: file, filename: 'formiii-signature1' }
        }).then(function (resp) {
            $scope.form.signature1 = resp.data.url
        }, function (resp) {
            alert("error uploading file");
            return;
        })
    }

    $scope.onRemoveSignature1File = function (file) {
        axios.post('file-remove', { url: $scope.form.signature1 })
            .then(function (response) {
                $scope.form.signature1 = null;
            })
    }

    $scope.formDisabled = function(){
        if(!$scope.form.signature || !$scope.form.signature1){
            return true;
        }
        return false;
    }

    


    $scope.onSubmit = function(event){
        event.preventDefault();
        event.stopPropagation();
        const data = _.cloneDeep($scope.form);
        axios.post('form-iii', data).then(function(){

        });
    }


    function onInit(){
        $scope.form.name = $scope.formd.name;
        $scope.form.license_number = $scope.params.license_number;
        $scope.form.form_ii_uuid = $scope.formii.uuid;
    }

}


FormiiiFormController.$inject = ['$scope', '$timeout', 'Upload'];
