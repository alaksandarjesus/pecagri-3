(function(){
    angular.module('sharedModule').controller('NavbarController', NavbarController);
})();

function NavbarController($scope){

    $scope.openMenu = ($mdMenu, ev)=>{
        $mdMenu.open(ev);
    }

}


NavbarController.$inject = ['$scope'];