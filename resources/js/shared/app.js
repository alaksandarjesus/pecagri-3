angular.module('sharedModule', ['ngMaterial','ngAria', 'ngAnimate', 'ngMessages', 'ngFileUpload']);

require('./config');
require('./NavbarController')
require('./AppController')