angular.module('sharedModule').run(axiosInterceptorFn);

function axiosInterceptorFn($rootScope, $mdToast){
    
    // Add a request interceptor
    window.axios.interceptors.request.use(function(config) {
        // Do something before request is sent
        $rootScope.$broadcast('apiBackdrop', {show:true});
        return config;
    }, function(error) {
        // Do something with request error
        return Promise.reject(error);
    });
    
    // Add a response interceptor
    window.axios.interceptors.response.use(function(response) {
        // Any status code that lie within the range of 2xx cause this function to trigger
        // Do something with response data
        $rootScope.$broadcast('apiBackdrop', {show:false});
        if (response.data.redirect) {
            window.location = response.data.redirect;
        }
        return response.data;
    }, function(error) {
        // Any status codes that falls outside the range of 2xx cause this function to trigger
        // Do something with response error
        const pinTo = 'top right';
        $rootScope.$broadcast('apiBackdrop', {show:false});
        if (error.response.status === 404) {
            $mdToast.show(
                $mdToast.simple()
                .textContent("Not found!, The url you are looking at is invalid")
                .position(pinTo)
                .toastClass('error')
                .hideDelay(3000));
    
        } else if (error.response.status === 500) {
            $mdToast.show(
                $mdToast.simple()
                .textContent("Server Error!, Server is down. Try again later")
                .position(pinTo)
                .toastClass('error')
                .hideDelay(3000));
    
        } else {
            let message = error.response.data.message;
            let errors = error.response.data.errors;
            if(!_.isEmpty(errors)){
                message = errors[Object.keys(errors)[0]][0];
            }
            $mdToast.show(
                $mdToast.simple()
                .textContent(message)
                .position(pinTo)
                .toastClass('error')
                .hideDelay(3000));
        }
        return Promise.reject(error.response.data);
    });
    }

    axiosInterceptorFn.$inject = ['$rootScope','$mdToast'];