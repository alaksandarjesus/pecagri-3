(function(){
    angular.module('view').controller('ActionController',ActionController);
})();


function ActionController($scope, $mdDialog, $mdToast){

    $scope.approveForm = function($event, type, uuid){
        event.preventDefault();
        event.stopPropagation();
        var confirm = $mdDialog.confirm()
        .title('Would you like to approve this ' + type + '?')
        .textContent('This action is irrevocable.')
        .ariaLabel('Confirm Action')
        .targetEvent(event)
        .ok('Yes!')
        .cancel('No');

    $mdDialog.show(confirm).then(function (reason) {
        processRequest('approve',type, uuid, null);
    }, function () {
        showToast('You cancelled the request', 'error');
    });
    }

    $scope.editForm = function($event, type, uuid){
        event.preventDefault();
        event.stopPropagation();
        var confirm = $mdDialog.prompt()
        .title('Would you like to mark this ' + type + ' to edit?')
        .textContent('This action is irrevocable.')
        .ariaLabel('Confirm Action')
        .targetEvent(event)
        .placeholder('Enter reason')
        .required(true)
        .ok('Yes!')
        .cancel('No');

    $mdDialog.show(confirm).then(function (reason) {
        processRequest('edit',type, uuid, reason);
        
    }, function () {
        showToast('You cancelled the request', 'error');
    });
    }

    $scope.rejectForm = function($event, type, uuid){
        event.preventDefault();
        event.stopPropagation();
        var confirm = $mdDialog.prompt()
        .title('Would you like to reject this ' + type + '?')
        .textContent('This action is irrevocable.')
        .ariaLabel('Confirm Action')
        .targetEvent(event)
        .placeholder('Enter reason')
        .required(true)
        .ok('Yes!')
        .cancel('No');

    $mdDialog.show(confirm).then(function (reason) {
        processRequest('reject',type, uuid, reason);
        
    }, function () {
        showToast('You cancelled the request', 'error');
    });
    }

    function processRequest(action, type, uuid, reason){
        axios.post(type+'/process', { action: action, uuid:uuid, reason:reason })
            .then(function (response) {
                showToast('You have successfully approved the request', 'success');
            });
    }

    function showToast(msg, className) {
        $mdToast.show(
            $mdToast.simple()
                .textContent(msg)
                .position('top right')
                .toastClass(className)
                .hideDelay(3000));
    }

    $scope.printForm = function($event){
        event.preventDefault();
        event.stopPropagation();
        window.print();
    }

}

ActionController.$inject = ['$scope', '$mdDialog', '$mdToast'];