@extends('layouts.dashboard')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4>Dashboard</h4>
                    
                    @if($user->can_apply_form_d || $user->can_process_form_d)
                    <div class="row">
                        <div class="col-sm-3">
                        @include('form-d.dashboard')
                        </div>
                    </div>
                    @endif
                    @if($user->can_apply_form_ii || $user->can_process_form_ii)
                   <div class="row">
                       <div class="col-sm-3">
                       @include('form-ii.dashboard')
                       </div>
                   </div>
                   @endif
                </div>
            </div>
        </div>
    </div>
</div>


@endsection