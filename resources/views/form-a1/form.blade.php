@extends('layouts.form-a1')

@section('content')

<div class="container-fluid" ng-controller="Forma1FormController" ng-cloak>
    @if(isset($form) && !empty($form))
    <div ng-init='form = @json($form)'></div>
    @endif
    <div class="row">
        <div class="col-sm-8 mx-auto">
            <div class="card">
                <div class="card-body">
                    @include('form-a1.shared.form-title')
                    <div class="row">
                        <div class="col-sm-12">
                            <form id="formA1" ng-submit="onSubmit($event)">
                                <div class="col-sm-12 col-md-12">
                                    <p>1.Details of the applicant</p>
                                    <p>a. Name of the Applicant</p>
                                    <md-input-container class="md-block">
                                        <label>Name of the Applicant</label>
                                        <input type="text" required name="name" ng-model="form.name">
                                        <div ng-messages="formA1.name.$error">
                                            <div ng-message="required">Name is required</div>
                                        </div>
                                    </md-input-container>
                                </div>
                                <p>b. Name of the Concern</p>
                                <md-input-container class="md-block">
                                    <label>Name of the Concern</label>
                                    <input type="text" required name="concern" ng-model="form.concern">
                                    <div ng-messages="formA1.concern.$error">
                                        <div ng-message="required">Name is required</div>
                                    </div>
                                </md-input-container>
                                <p>c. Postal address with telephone Number</p>
                                <md-input-container class="md-block">
                                    <label>Postal address</label>
                                    <textarea ng-model="form.address" required name="address" md-maxlength="150"
                                        rows="5" md-select-on-focus aria-label="Address"></textarea>
                                    <div ng-messages="formA1.address.$error">
                                        <div ng-message="required">Address is required</div>
                                    </div>
                                </md-input-container>
                                <md-input-container class="md-block">
                                    <label>telephone Number</label>
                                    <input type="text" required name="telephone" ng-model="form.telephone">
                                    <div ng-messages="formA1.telephone.$error">
                                        <div ng-message="required">Name is required</div>
                                    </div>
                                </md-input-container>
                                <p>2. Place of business (Please Give Full Address)</p>
                                <p>(i) For Sale:</p>
                                <md-input-container class="md-block">
                                    <label>Sale address</label>
                                    <textarea ng-model="form.saleAddress" required name="saleAddress" md-maxlength="150"
                                        rows="5" md-select-on-focus aria-label="saleAddress"></textarea>
                                    <div ng-messages="formA1.saleAddress.$error">
                                        <div ng-message="required">Address is required</div>
                                    </div>
                                </md-input-container>
                                <p>(i) For Storage:</p>
                                <md-input-container class="md-block">
                                    <label>Storage address</label>
                                    <textarea ng-model="form.storageAddress" required name="storageAddress"
                                        md-maxlength="150" rows="5" md-select-on-focus
                                        aria-label="storageAddress"></textarea>
                                    <div ng-messages="formA1.storageAddress.$error">
                                        <div ng-message="required">Address is required</div>
                                    </div>
                                </md-input-container>
                                <p>3. Whether the Application is for: {{strtolower(session()->get('user')->role->name)}}
                                </p>
                                <p>4. Details of fertilizer and their source in Form “O” *;</p>
                                <div>
                                    <md-input-container class="md-block">
                                        <label>Name of fertilizer</label>
                                        <input type="text" required name="fertilizerName"
                                            ng-model="form.fertilizerName">
                                        <div ng-messages="formA1.fertilizerName.$error">
                                            <div ng-message="required">Name is required</div>
                                        </div>
                                    </md-input-container>
                                    <md-radio-group ng-model="form.certifiateSource"
                                        class="md-block d-flex justify-content-start align-items-center">
                                        <label>Whether certificate of source in form “O” is attached?</label>
                                        <md-radio-button value="Yes" class="md-primary">Yes</md-radio-button>
                                        <md-radio-button value="No" class="ml-4"> No </md-radio-button>
                                    </md-radio-group>
                                </div>
                                <div>
                                    <md-input-container class="md-block">
                                        <label>Name of fertilizer</label>
                                        <input type="text" required name="fertilizerName1"
                                            ng-model="form.fertilizerName1">
                                        <div ng-messages="formA1.fertilizerName1.$error">
                                            <div ng-message="required">Name is required</div>
                                        </div>
                                    </md-input-container>
                                    <md-radio-group ng-model="form.certifiateSource1"
                                        class="md-block d-flex justify-content-start align-items-center">
                                        <label>Whether certificate of source in form “O” is attached?</label>
                                        <md-radio-button value="Yes" class="md-primary">Yes</md-radio-button>
                                        <md-radio-button value="No" class="ml-4"> No </md-radio-button>
                                    </md-radio-group>
                                </div>
                                <div ng-controller="SelectedTextController" class="md-padding" ng-cloak>
                                    <h1 class="md-title">Payment Mode</h1>
                                    <div layout="row">
                                        <md-input-container>
                                            <label>Payment Option</label>
                                            <md-select ng-model="selectedItem" md-selected-text="getSelectedText()">
                                                <md-optgroup label="items">
                                                    <md-option ng-value="bank">Bank/Treasury</md-option>
                                                    <md-option ng-value="dd">Demand Draft</md-option>
                                                </md-optgroup>
                                            </md-select>
                                        </md-input-container>
                                    </div>
                                </div>
                                <p>6. Whether the intimation is for authorization letter or a renewal thereof. <br>
                                    (Note: In case the intimation is for the renewal of authorization letter, the
                                    acknowledgment in Form A2 should be submitted for necessary endorsement
                                    thereon.)
                                </p>
                                <md-radio-group ng-model="form.renewal"
                                    class="md-block d-flex justify-content-start align-items-center">
                                    <md-radio-button value="Yes" class="md-primary">Yes</md-radio-button>
                                    <md-radio-button value="No" class="ml-4"> No </md-radio-button>
                                </md-radio-group>
                                <div>
                                    <ul class="list-group">
                                        <li ng-repeat="document in form.documents"
                                            class="list-group-item d-flex justify-content-between align-items-center">
                                            <a ng-href="document.url" target="_blank">@{{document.filename}}</a>
                                            <md-button class="md-icon-button float-right" aria-label="Remove"
                                                ng-click="onRemoveFile(document)">
                                                <md-icon class="text-danger">delete_outline</md-icon>
                                            </md-button>
                                        </li>
                                    </ul>
                                    <md-button class="md-raised md-primary btn-md btn-block float-right" type="button"
                                        ngf-pattern="'image/*,application/pdf'" ngf-select="uploadMultipleFiles($files)"
                                        ng-model="documents" ngf-multiple="true">Upload Documents</md-button>
                                </div>
                                <p>7. Any other relevant information <br>
                                    I have read the terms and conditions of eligibility for submission of
                                    Memorandum of Intimation and undertake that the same will be compiled by
                                    me and in token of the same; I have signed the same and is enclosed herewith
                                </p>
                                <md-input-container class="md-block">
                                    <label>Relevant Information</label>
                                    <textarea ng-model="form.relevantInfo" required name="relevantInfo"
                                        md-maxlength="150" rows="5" md-select-on-focus aria-label="relevantInfo"
                                        value="NILL"></textarea>
                                    <div ng-messages="formA1.relevantInfo.$error">
                                        <div ng-message="required">Relevant Info is required</div>
                                    </div>
                                </md-input-container>
                                <div class="d-flex justify-content-between align-items-end">
                                    <div>
                                        <md-input-container class="md-block mt-2">
                                            <label>Place</label>
                                            <input type="text" required name="application_place"
                                                ng-model="form.application_place">
                                            <div ng-messages="formA1.application_place.$error">
                                                <div ng-message="required">Application Place is required</div>
                                            </div>
                                        </md-input-container>
                                        <md-input-container class="md-block mt-2">
                                            <label>Date</label>
                                            <input type="text" required name="date" ng-model="form.date" readonly>
                                            <div ng-messages="formA1.date.$error">
                                                <div ng-message="required">Date is required</div>
                                            </div>
                                        </md-input-container>
                                    </div>
                                    <div>
                                        <img ng-if="form.signature" class="signature" ng-src="@{{form.signature}}"
                                            alt="">
                                        <md-button ng-if="!form.signature"
                                            class="md-raised md-primary btn-md btn-block float-right" type="button"
                                            ngf-pattern="'image/*,application/pdf'"
                                            ngf-select="uploadSignatureFile($file)" ng-model="signature"
                                            ngf-multiple="false">Signature of the Applicant</md-button>
                                        <md-button ng-if="form.signature" class="btn-block md-raised md-accent"
                                            ng-click="onRemoveSignatureFile()">Remove Signature</md-button>
                                    </div>
                                </div>
                                @include('form-a1.shared.terms-and-conditions')
                                <div class="d-flex justify-content-between align-items-end">
                                    <div>
                                        <md-input-container class="md-block mt-2">
                                            <label>Place</label>
                                            <input type="text" required name="place" ng-model="form.place">
                                            <div ng-messages="formA1.place.$error">
                                                <div ng-message="required">Application Place is required</div>
                                            </div>
                                        </md-input-container>
                                        <md-input-container class="md-block mt-2">
                                            <label>Date</label>
                                            <input type="text" required name="application_date"
                                                ng-model="form.application_date" readonly>
                                            <div ng-messages="formA1.application_date.$error">
                                                <div ng-message="required">Date is required</div>
                                            </div>
                                        </md-input-container>
                                    </div>
                                    <div>
                                        <img ng-if="form.signature1" class="signature1" ng-src="@{{form.signature1}}"
                                            alt="">
                                        <md-button ng-if="!form.signature1"
                                            class="md-raised md-primary btn-md btn-block float-right" type="button"
                                            ngf-pattern="'image/*,application/pdf'"
                                            ngf-select="uploadSignature1File($file)" ng-model="signature1"
                                            ngf-multiple="false">Signature of the Applicant</md-button>
                                        <md-button ng-if="form.signature1" class="btn-block md-raised md-accent"
                                            ng-click="onRemoveSignature1File()">Remove Signature</md-button>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-sm-12">
                                        <md-button class="md-raised md-primary float-right" type="submit" ng-disabled="formA1.$invalid || formDisabled()">Submit</md-button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

@endsection