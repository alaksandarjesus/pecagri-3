@extends('layouts.form-a1')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h5>Form A1</h5>
                    @include('form-a1.table')
                </div>
            </div>

        </div>
    </div>
</div>



@endsection