<div>
    <h4 class="text-center">TERMS AND CONDITION OF AUTHORISATION</h4>
    <p>1. I shall comply with the provisions of the Fertilizer (Control) Order, 1985
        and the notifications issued thereunder for the time being in force.
    </p>
    <p>2. I shall from time to time report to the Notified Authority and inform
        about change in the premises of sale depot and godowns attached to sale
        Depot.
    </p>
    <p>3. I shall also submit in time all the returns as may be prescribed by the
        State Government.
    </p>
    <p>4. I shall, not sell fertilizers for industrial use.</p>
    <p>5. I shall file a separate Memorandum of Intimation (MOI) for, where the
        storage point is located outside the area jurisdiction of the Notified
        Authority where the sale depot is located.
    </p>
    <p>6. I shall file separate MOI for each place when the business of selling
        Fertilizers is intended to be carried on at more than one place.
    </p>
    <p>7. I shall file separate MOI if I carry on the business of fertilizers both as
        retail and wholesale dealer.
    </p>
    <p>8. I confirm that my previous certificate of Registration or Authorization
        is not under Suspension or Cancellation or debarred from selling of
        fertilizers.
    </p>
</div>
<div>
    <h4 class="text-center">Declaration</h4>
    <p>I/We declare that the information given above is true to the best of
        my/our knowledge and belief and no part thereof is false or no material
        has been concealed.</p>
</div>