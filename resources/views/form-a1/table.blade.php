

<table class="table table-bordered">
    <thead>
        <tr>
            <th>UUID</th>
            <th>AO</th>
            <th>DDA</th>
            <th>JDA</th>
            <th>ADA (Authority)</th>
        </tr>
    </thead>
    <tbody ng-cloak>
        @foreach($forms as $form)
            <tr>
                <td>
                    <div>
                    @if($form->can_edit)
                        <a href="{{url('form-a1/'.$form->uuid.'/edit')}}" class="text-danger mr-2" target="_blank">Edit Form</a>
                    @endif
                   
                    
                    <a href="{{url('form-a1/'.$form->uuid)}}" target="_blank">View / Print Form</a>
                   
                    @if($form->generate_form_a2)
                        <a href="{{url('form-a1/'.$form->uuid.'/form-a2')}}" class="ml-4 text-danger mr-2" target="_blank">Generate Form A2</a>
                    @endif
                    @if($form->form_a2_id )
                        <a href="{{url('form-a1/'.$form->uuid.'/form-a2/view')}}" class="ml-4 text-danger mr-2" target="_blank">Download Form A2</a>
                    @endif
                    </div>
                    <div>
                        <small>{{$form->updated_at}}</small>
                    </div>
                </td>
                <td>
                  <div>@include('shared.form.status', ['verified_at' =>$form->process->verified_by_ao , 'reject_reason' => $form->process->rejected_by_ao])</div>
                </td>
                <td>
                  <div>@include('shared.form.status', ['verified_at' =>$form->process->verified_by_dda , 'reject_reason' => $form->process->rejected_by_dda])</div>
                </td>
                <td>
                  <div>@include('shared.form.status', ['verified_at' =>$form->process->verified_by_jda , 'reject_reason' => $form->process->rejected_by_jda])</div>
                </td>
                <td>
                  <div>@include('shared.form.status', ['verified_at' =>$form->process->verified_by_adaa , 'reject_reason' => $form->process->rejected_by_adaa])</div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>