@extends('layouts.view')

@section('action')

@include('shared.form.action', ['type' => 'form-a1'])

@endsection

@section('content')

@include('form-a1.shared.view-title')

<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <p>1. Details Of the Applicant</p>
                <p>a. Name Of the Applicant : {{$form->name}}</p>
            </div>
        </div>
    </div>
</div>

@endsection