@extends('layouts.form-a2')

@section('content')

<div class="container-fluid" ng-controller="Forma2FormController" ng-cloak>
    <div class="row">
        <div class="col-sm-8 mx-auto">
            <div class="card">
                <div class="card-body">
                    @include('form-a2.shared.form-title')
                    <div class="row">
                        <div class="col-sm-12">
                            <form name="formA2" ng-controller="Forma2FormController" ng-submit="onSubmit($event)"
                                ng-init='forma2 = @json($form); params = @json($params)'>
                                <div class="col-sm-12 col-md-12">
                                    <p>Received from <strong>M/s. @{{form.concern}}</strong> a complete Memorandum of
                                        Intimation along with Form O, fee of Rs. 1250/- remitted at SBI, Puducherry vide
                                        Challan bearing number
                                    </p>
                                    <md-input-container class="md-block">
                                        <label>challan bearing number</label>
                                        <input type="text" required name="challanBearingNo"
                                            ng-model="form.challanBearingNo">
                                        <div ng-messages="formA2.challanBearingNo.$error">
                                            <div ng-message="required">Challan Bearing No. is required</div>
                                        </div>
                                    </md-input-container>
                                    <div class="col-sm-6 col-md-6">
                                        <p>dated</p>
                                        <md-input-container class="md-block">
                                            <label>dated</label>
                                            <input type="text" required name="challanDate" ng-model="form.challanDate">
                                            <div ng-messages="formA2.challanDate.$error">
                                                <div ng-message="required">Challan Date is required</div>
                                            </div>
                                        </md-input-container>
                                    </div>
                                    <div class="col-sm-12 col-md-12">
                                        <p>This acknowledgement shall be deemed to be the letter of authorization
                                            entitling the applicant to carry on the business as applied for, for a
                                            period of 3 years from the date of issue of this Memo of acknowledgement
                                            unless suspended or revoked by the competent authority.
                                        </p>
                                    </div>
                                    <div>
                                        <md-input-container class="md-block mt-2">
                                            <label>Date</label>
                                            <input type="text" required name="date" ng-model="form.date" readonly>
                                            <div ng-messages="formA2.date.$error">
                                                <div ng-message="required">Date is required</div>
                                            </div>
                                        </md-input-container>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col-sm-12 col-md-12">
                                            <p>License No. : @{{form.license_number}}</p>
                                            <p>Date of issue : @{{form.date_of_issue}}</p>
                                            <p>Date pf ex[ory : @{{form.date_of_validity}}</p>
                                            <p>Place : Puducherry <br>
                                                Principals represented: As enclosed</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 offset-sm-8">
                                            <img ng-if="form.signature" class="signature" ng-src="@{{form.signature}}"
                                                alt="">
                                            <md-button ng-if="!form.signature"
                                                class="md-raised md-primary btn-md btn-block float-right" type="button"
                                                ngf-pattern="'image/*,application/pdf'"
                                                ngf-select="uploadSignatureFile($file)" ng-model="signature"
                                                ngf-multiple="false">Signature Of Notified Authority</md-button>
                                            <md-button ng-if="form.signature" class="btn-block md-raised md-accent"
                                                ng-click="onRemoveSignatureFile()">Remove Signature</md-button>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection