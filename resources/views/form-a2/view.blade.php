@extends('layouts.view')

@section('action')

@include('shared.form.action')

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    @include('form-a2.shared.form-title')
                    <div class="col-sm-12 col-md-12">
                        <p>1. Received from M/s. <strong>@{{$form.concern}}</strong> a complete Memorandum of
                            Intimation along with Form O, fee of Rs. 1250/- remitted at SBI, Puducherry vide
                            Challan bearing number <strong>@{{$form.challanBearingNo}}</strong> dated
                            @{{$form.challanDate}}
                        </p>
                        <p>2. This acknowledgement shall be deemed to be the letter of authorization entitling the
                            applicant to carry on the business as applied for, for a
                            period of 5 years from the date of issue of this Memo of acknowledgement unless
                            suspended or revoked by the competent authority.
                        </p>
                    </div>
                </div>
                <div class="d-flex justify-content-between align-items-end">
                    <div>
                        <p>Date : @{{$form.date}}</p>
                    </div>
                    <div>
                        @{{$form.signature}}<br> SIGNATURE OF NOTIFIED AUTHORITY
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    License No : @{{$form.license_number}} <br>
                    Date Of Issue : @{{$form.date_of_issue}} <br>
                    Date Of Ecpiry : @{{$form.date_of_validity}} <br>
                    Place : Puducherry
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection