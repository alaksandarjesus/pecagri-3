@extends('layouts.form-d')

@section('content')

<div class="container-fluid" ng-controller="FormdFormController" ng-cloak>
    @if(isset($form) && !empty($form))
        <div ng-init='form = @json($form)'></div>
    @endif
    <div class="row">
        <div class="col-sm-8 mx-auto">
            <div class="card">
                <div class="card-body">
                    @include('form-d.shared.form-title')
                    <div class="row">
                        <div class="col-sm-12">
                            <form name="formD" ng-submit="onSubmit($event)">
                                <div class="col-sm-6 col-md-6">
                                <p>To</p>
                                <p>The Registering Authority, <br>
                                Department Of Agriculture & Farmers welfare, <br>
                                Puducherry.
                                </p>
                                </div>
                                <p>1. Full name</p>
                                <md-input-container class="md-block">
                                    <input type="text" required name="name" ng-model="form.name">
                                    <div ng-messages="formD.name.$error">
                                        <div ng-message="required">Name is required</div>
                                    </div>
                                </md-input-container>
                                <p>Address</p>
                                <md-input-container class="md-block">
                                    <textarea ng-model="form.address" required name="address" md-maxlength="150"
                                        rows="5" md-select-on-focus aria-label="Address"></textarea>
                                    <div ng-messages="formD.address.$error">
                                        <div ng-message="required">Address is required</div>
                                    </div>
                                </md-input-container>
                                <p>Name Of the Concern</p>
                                <md-input-container class="md-block">
                                    <textarea ng-model="form.concern" required name="concern" md-maxlength="150"
                                        rows="5" md-select-on-focus aria-label="concern"></textarea>
                                    <div ng-messages="formD.concern.$error">
                                        <div ng-message="required">Concern Name is required</div>
                                    </div>
                                </md-input-container>
                                <p>2. Does the applicant possess the qualification prescribed by the State Government under
                                    Sub-clause (I) of clause 14 of the Fertilizer (Control) Order, 1985</p>
                                <md-radio-group ng-model="form.qualification"
                                    class="md-block d-flex justify-content-start align-items-center">
                                    <md-radio-button value="Yes" class="md-primary">Yes</md-radio-button>
                                    <md-radio-button value="No" class="ml-4"> No </md-radio-button>
                                </md-radio-group>
                                <p>3. Is the applicant a new comer?</p>
                                <md-radio-group ng-model="form.new_comer"
                                    class="md-block d-flex justify-content-start align-items-center">
                                    <md-radio-button value="Yes" class="md-primary">Yes</md-radio-button>
                                    <md-radio-button value="No" class="ml-4"> No </md-radio-button>
                                </md-radio-group>
                                <p>4. Situation of the applicant’s premises where physical/ granulated/ special mixture of
                                    fertilizers/ organic fertilizer/ biofertilizer will be prepared</p>
                                <md-input-container class="md-block">
                                    <textarea ng-model="form.premises" required name="premises" md-maxlength="150"
                                        rows="5" md-select-on-focus aria-label="Premises"></textarea>
                                    <div ng-messages="formD.premises.$error">
                                        <div ng-message="required">Premises is required</div>
                                    </div>
                                </md-input-container>
                                <p>5. Full particulars regarding specifications of the physical/ granulated/ special
                                    mixture of fertilizers/ organic fertilizer/ biofertilizer for which the certificate
                                    is required and the raw materials used in making the mixture</p>
                                <md-input-container class="md-block">
                                    <textarea ng-model="form.particulars" required name="particulars" md-maxlength="150"
                                        rows="5" md-select-on-focus aria-label="Particulars"></textarea>
                                    <div ng-messages="formD.particulars.$error">
                                        <div ng-message="required">Particulars is required</div>
                                    </div>
                                </md-input-container>
                                <p>6. Full particulars of any other certificate of manufacture, if any, issued by any other
                                    Registering Authority</p>
                                <md-input-container class="md-block">
                                    <textarea ng-model="form.certificates" required name="certificates"
                                        md-maxlength="150" rows="5" md-select-on-focus
                                        aria-label="Certificates"></textarea>
                                    <div ng-messages="formD.certificates.$error">
                                        <div ng-message="required">Certificates is required</div>
                                    </div>
                                </md-input-container>
                                <p>7. How long has the applicant been carrying on the business of preparing
                                    {{strtolower(session()->get('user')->role->name)}}?</p>
                                <md-input-container class="md-block">
                                    <textarea ng-model="form.duration" required name="duration" md-maxlength="150"
                                        rows="5" md-select-on-focus aria-label="Duration"></textarea>
                                    <div ng-messages="formD.duration.$error">
                                        <div ng-message="required">Duration is required</div>
                                    </div>
                                </md-input-container>
                                <p>8. Quantities of each physical/ granulated/
                                    special mixture of fertilizers/ mixture of micronutrient
                                    fertilizers/ organic fertilizers/ biofertilizers (in tonnes)
                                    in my/out possession on the date of the Application and field
                                    at different addresses noted against each</p>
                                <md-input-container class="md-block">
                                    <textarea ng-model="form.quantity" required name="quantity" md-maxlength="150"
                                        rows="5" md-select-on-focus aria-label="Quantity"></textarea>
                                    <div ng-messages="formD.quantity.$error">
                                        <div ng-message="required">Quantity is required</div>
                                    </div>
                                </md-input-container>
                                <p>9. a) If the applicant has been carrying on the business of preparing
                                    {{strtolower(session()->get('user')->role->name)}}, give all particulars of such
                                    Mixtures handled, the period and the place(s) at which the mixing of Fertilizers was
                                    done.</p>
                                <md-input-container class="md-block">
                                    <textarea ng-model="form.business" required name="business" md-maxlength="150"
                                        rows="5" md-select-on-focus aria-label="Business"></textarea>
                                    <div ng-messages="formD.business.$error">
                                        <div ng-message="required">Business is required</div>
                                    </div>
                                </md-input-container>
                                <p>b) Also give the quantities of physical/ granulated/ special mixture of
                                    {{strtolower(session()->get('user')->role->name)}}, handled during the past Calendar
                                    year</p>
                                <md-input-container class="md-block">
                                    <textarea ng-model="form.quantities" required name="quantities" md-maxlength="150"
                                        rows="5" md-select-on-focus aria-label="Quantities"></textarea>
                                    <div ng-messages="formD.quantities.$error">
                                        <div ng-message="required">Quantity is required</div>
                                    </div>
                                </md-input-container>
                                <p>10. If the application is for renewal, indicate briefly why the original certificate
                                    could not be acted on within the period of its validity</p>
                                <md-input-container class="md-block">
                                    <textarea ng-model="form.renewal" required name="renewal" md-maxlength="150"
                                        rows="5" md-select-on-focus aria-label="Renewal"></textarea>
                                    <div ng-messages="formD.renewal.$error">
                                        <div ng-message="required">Renewal is required</div>
                                    </div>
                                </md-input-container>
                                <p>11. Name and address of the person requiring the special mixture of fertilizers</p>
                                <md-input-container class="md-block">
                                    <textarea ng-model="form.requiring" required name="requiring" md-maxlength="150"
                                        rows="5" md-select-on-focus aria-label="Requiring"></textarea>
                                    <div ng-messages="formD.requiring.$error">
                                        <div ng-message="required">Name and address is required</div>
                                    </div>
                                </md-input-container>
                                <div>
                                    <ul class="list-group">
                                        <li ng-repeat="document in form.documents"
                                            class="list-group-item d-flex justify-content-between align-items-center">
                                            <a ng-href="document.url" target="_blank">@{{document.filename}}</a>
                                            <md-button class="md-icon-button float-right" aria-label="Remove"
                                                ng-click="onRemoveFile(document)">
                                                <md-icon class="text-danger">delete_outline</md-icon>
                                            </md-button>
                                        </li>
                                    </ul>
                                    <md-button class="md-raised md-primary btn-md btn-block float-right" type="button"
                                        ngf-pattern="'image/*,application/pdf'" ngf-select="uploadMultipleFiles($files)"
                                        ng-model="documents" ngf-multiple="true">Upload Documents</md-button>
                                </div>
                                @include('form-d.shared.declaration')

                                <div>
                                <p>
                                Name and address of applicant:
                                </p>
                                    <md-input-container class="md-block mt-2">
                                        <label>Applicant Name</label>
                                        <input type="text" required name="applicant_name"
                                            ng-model="form.applicant_name">
                                        <div ng-messages="formD.applicant_name.$error">
                                            <div ng-message="required">Applicant Name is required</div>
                                        </div>
                                    </md-input-container>
                                    <md-input-container class="md-block">
                                        <label for="">Applicant Address</label>
                                        <textarea ng-model="form.applicant_address" required name="applicant_address"
                                            md-maxlength="150" rows="5" md-select-on-focus
                                            aria-label="Applicant Address"></textarea>
                                        <div ng-messages="formD.applicant_address.$error">
                                            <div ng-message="required">Applicant address is required</div>
                                        </div>
                                    </md-input-container>
                                </div>
                                <div class="d-flex justify-content-between align-items-end">
                                    <div>
                                        <md-input-container class="md-block mt-2">
                                            <label>Place</label>
                                            <input type="text" required name="application_place" ng-model="form.application_place">
                                            <div ng-messages="formD.application_place.$error">
                                                <div ng-message="required">Application Place is required</div>
                                            </div>
                                        </md-input-container>
                                        <md-input-container class="md-block mt-2">
                                            <label>Date</label>
                                            <input type="text" required name="date" ng-model="form.date" readonly>
                                            <div ng-messages="formD.date.$error">
                                                <div ng-message="required">Date is required</div>
                                            </div>
                                        </md-input-container>
                                    </div>
                                    <div>
                                    <img ng-if="form.signature" class="signature" ng-src="@{{form.signature}}" alt="">
                                    <md-button ng-if="!form.signature" class="md-raised md-primary btn-md btn-block float-right" type="button"
                                        ngf-pattern="'image/*,application/pdf'" ngf-select="uploadSignatureFile($file)"
                                        ng-model="signature" ngf-multiple="false">Upload Signature</md-button>
                                    <md-button ng-if="form.signature" class="btn-block md-raised md-accent" ng-click="onRemoveSignatureFile()">Remove Signature</md-button>
                                </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-sm-12">
                                        <md-button class="md-raised md-primary float-right" type="submit" ng-disabled="formD.$invalid || formDisabled()">Submit</md-button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection