<div>
<h4 class="text-center">Declaration</h4>
<p>(a) I have deposited the prescribed registration certificate fee/renewal fee.</p>

<p>(b) I/We declare that the information given above is true and correct to the best of my/ our knowledge and better, and no part there is false.</p>

<p>(c) I/We have carefully read the terms and conditions of the certificate of manufacture given in Form F appended to the Fertilizer (Control) Order, 1985 and agree to abide by them.</p>

<p>(d) I/We declare that the physical/ granulated/ special mixture of fertilizer/ organic fertilizer/ biofertilizer for which certificate of manufacture is applied for shall be prepared by me/us or
     by a person having such qualifications as may be prescribed by the State Government from time to time or by any other person under my/ our direction, supervision and control or under the direction,
      supervision and control or person having the said qualification.</p>

<p>(e) I/We declare that the requisite laboratory facility specified by the Controller, under this Order is possessed by me/us.</p>

<p>(f) I am enclosing an attested copy of the requisition made by the purchaser of the special mixture of fertilizers.</p>
</div>