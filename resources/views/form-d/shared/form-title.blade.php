<div class="row">
    <div class="col-sm-12">
    <h4 class="text-center">Form 'D'</h4>
    <h5 class="text-center">[See clause 14(2) and 18(1)]</h5>
    @if(session()->has('user'))
    <h5  class="text-center">FORM OF APPLICATION TO OBTAIN A CERTIFICATE OF MANUFACTURE OF</h5>
    <h5 class="text-center">{{strtoupper(session()->get('user')->role->name)}}</h5>
    @endif
    </div>
</div>