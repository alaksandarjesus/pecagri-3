

<table class="table table-bordered">
    <thead>
        <tr>
            <th>UUID</th>
            <th>AO</th>
            <th>DDA</th>
            <th>JDA</th>
            <th>ADA</th>
            <th>Director</th>
        </tr>
    </thead>
    <tbody ng-cloak>
        @foreach($forms as $form)
            <tr>
                <td>
                    <div>
                    @if($form->can_edit)
                        <a href="{{url('form-d/'.$form->uuid.'/edit')}}" class="text-danger mr-2" target="_blank">Edit Form</a>
                    @endif
                   
                    
                    <a href="{{url('form-d/'.$form->uuid)}}" target="_blank">View / Print Form</a>
                   
                    @if($form->generate_form_f)
                        <a href="{{url('form-d/'.$form->uuid.'/form-f')}}" class="ml-4 text-danger mr-2" target="_blank">Generate Form F</a>
                    @endif
                    @if($form->form_f_id )
                        <a href="{{url('form-d/'.$form->uuid.'/form-f/view')}}" class="ml-4 text-danger mr-2" target="_blank">Download Form F</a>
                    @endif
                    </div>
                    <div>
                        <small>{{$form->updated_at}}</small>
                    </div>
                </td>
                <td>
                  <div>@include('shared.form.status', ['verified_at' =>$form->process->verified_by_ao , 'reject_reason' => $form->process->rejected_by_ao])</div>
                </td>
                <td>
                  <div>@include('shared.form.status', ['verified_at' =>$form->process->verified_by_dda , 'reject_reason' => $form->process->rejected_by_dda])</div>
                </td>
                <td>
                  <div>@include('shared.form.status', ['verified_at' =>$form->process->verified_by_jda , 'reject_reason' => $form->process->rejected_by_jda])</div>
                </td>
                <td>
                  <div>@include('shared.form.status', ['verified_at' =>$form->process->verified_by_ada , 'reject_reason' => $form->process->rejected_by_ada])</div>
                </td>
                <td>
                  <div>@include('shared.form.status', ['verified_at' =>$form->process->verified_by_director , 'reject_reason' => $form->process->rejected_by_director])</div>
                
                </td>
            </tr>
        @endforeach
    </tbody>
</table>