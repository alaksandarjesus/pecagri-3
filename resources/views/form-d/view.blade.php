@extends('layouts.view')

@section('action')

@include('shared.form.action', ['type' => 'form-d'])

@endsection

@section('content')

@include('form-d.shared.view-title')
<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <p>To</p>
                    <p>The Registering Authority, <br>
                        Department Of Agriculture & Farmers welfare, <br>
                        Puducherry.
                    </p>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-12 col-md-6"> 1. Full name address of the applicant.
                </label>
                <div class="col-sm-12 col-md-6">
                    <div><span>{{$form->name}}</span>, <br>{{$form->address}}</div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-12 col-md-6"> Name of the concern.
                </label>
                <div class="col-sm-12 col-md-6">
                    <div>{{$form->concern}}</div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-12 col-md-6"> 2. Does the applicant possess the qualification prescribed by the
                    State Government under Sub-clause (I) of clause 14 of the Fertilizer (Control) Order, 1985:
                </label>
                <div class="col-sm-12 col-md-6">
                    <div>{{$form->qualification}}</div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-12 col-md-6"> 3. Is the applicant a new comer?
                </label>
                <div class="col-sm-12 col-md-6">
                    <div>{{$form->new_comer}}</div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-12 col-md-6"> 4. Situation of the applicant’s premises where
                    {{strtolower($form->owner->role->name)}}
                    will be prepared.
                </label>
                <div class="col-sm-12 col-md-6">
                    <div>{{$form->premises}}</div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-12 col-md-6"> 5. Full particulars regarding specifications of
                    the {{strtolower($form->owner->role->name)}} for which the
                    certificate is required and the raw materials used in making the mixture.
                </label>
                <div class="col-sm-12 col-md-6">
                    <div>{{$form->particulars}}</div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-12 col-md-6"> 6. Full particulars of any other certificate of
                    manufacture, if any, issued by any other Registering Authority.
                </label>
                <div class="col-sm-12 col-md-6">
                    <div>{{$form->certificates}}</div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-12 col-md-6"> 7. How long has the applicant been carrying on the business of
                    preparing {{strtolower($form->owner->role->name)}}?
                </label>
                <div class="col-sm-12 col-md-6">
                    <div>{{$form->duration}}</div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-12 col-md-6"> 8. Quantities of {{strtolower($form->owner->role->name)}} (in tonnes)
                    in my/out possession on the date of the Application and field at different addresses noted against
                    each.
                </label>
                <div class="col-sm-12 col-md-6">
                    <div>{{$form->quantity}}</div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-12 col-md-6"> 9. a) If the applicant has been carrying on the business of preparing
                    {{strtolower($form->owner->role->name)}}, give all particulars of such Mixtures handled, the period
                    and the place(s) at which the mixing of Fertilizers was done.
                </label>
                <div class="col-sm-12 col-md-6">
                    <div>{{$form->business}}</div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-12 col-md-6"> 9. b) Also give the quantities of
                    {{strtolower($form->owner->role->name)}}, handled during
                    the past Calendar year
                </label>
                <div class="col-sm-12 col-md-6">
                    <div>{{$form->quantities}}</div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-12 col-md-6"> 10. If the application is for renewal, indicate
                    briefly why the original certificate could not be acted on within the period of its
                    validity.
                </label>
                <div class="col-sm-12 col-md-6">
                    <div>{{$form->renewal}}</div>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-12 col-md-6"> 11. Name and address of the person requiring
                    the special mixture of fertilizers.
                </label>
                <div class="col-sm-12 col-md-6">
                    <div>{{$form->requiring}}</div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h4>Documents</h4>
                    @foreach($form->documents as $document)
                    <a href="{{$document->url}}">{{$document->filename}}</a>
                    @endforeach
                </div>
            </div>
            @include('form-d.shared.declaration')
            <div class="row">
                <label class="col-sm-12 col-md-6"> Name and address of applicant.
                </label>
                <div class="col-sm-12 col-md-6">
                    <div>{{$form->applicant_name}}, <br>{{$form->applicant_address}}</div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div><strong>Place: </strong>{{$form->application_place}}</div>
                </div>
                <div class="col-sm-12">
                    <div><strong>Date: </strong>{{$form->date}}</div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="float-right">
                        {{$form->signature}}
                        <!-- <img src="{{$form->signature}}" alt=""> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection