@extends('layouts.form-f')

@section('content')

<div class="container-fluid" ng-controller="FormfFormController" ng-cloak>
    <div class="row">
        <div class="col-sm-8 mx-auto">
            <div class="card">
                <div class="card-body">
                    <h4>Form F</h4>
                    @include('form-f.shared.form-title')
                    <div class="row">
                        <div class="col-sm-12">
                            <form name="formF" ng-controller="FormfFormController" ng-submit="onSubmit($event)" ng-init='formd = @json($form); params = @json($params)'>
                                <md-input-container class="md-block">
                                    <label>Certificate Number</label>
                                    <input type="text" required name="certificate_number"
                                        ng-model="form.certificate_number" readonly>
                                                                          <div ng-messages="formF.certificate_number.$error">
                                        <div ng-message="required">Certificate Number is required</div>
                                    </div>
                                </md-input-container>
                                <div class="row">
                                    <div class="col-sm-6">
                                    <md-input-container class="md-block">
                                    <label>Date of renewal</label>
                                    <input type="text" required name="date_of_renewal"
                                        ng-model="form.date_of_renewal" readonly>
                                    <div ng-messages="formF.date_of_renewal.$error">
                                        <div ng-message="required">Date of renewal is required</div>
                                    </div>
                                </md-input-container>
                                    </div>
                                    <div class="col-sm-6">
                                    <md-input-container class="md-block">
                                    <label>Valid upto</label>
                                    <input type="text" required name="valid_upto"
                                        ng-model="form.valid_upto" readonly>
                                    <div ng-messages="formF.valid_upto.$error">
                                        <div ng-message="required">Valid upto is required</div>
                                    </div>
                                </md-input-container>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">


                                    </div>

                                </div>
                                

                                <div>
                                    <img ng-if="form.signature" class="signature" ng-src="@{{form.signature}}" alt="">
                                    <md-button ng-if="!form.signature"
                                        class="md-raised md-primary btn-md btn-block float-right" type="button"
                                        ngf-pattern="'image/*,application/pdf'" ngf-select="uploadSignatureFile($file)"
                                        ng-model="signature" ngf-multiple="false">Upload Signature</md-button>
                                    <md-button ng-if="form.signature" class="btn-block md-raised md-accent"
                                        ng-click="onRemoveSignatureFile()">Remove Signature</md-button>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-sm-12">
                                        <md-button class="md-raised md-primary float-right" type="submit"
                                            ng-disabled="formF.$invalid || formDisabled()">Submit</md-button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection