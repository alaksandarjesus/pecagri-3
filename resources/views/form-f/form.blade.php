@extends('layouts.form-f')

@section('content')

<div class="container-fluid" ng-controller="FormfFormController" ng-cloak>
    <div class="row">
        <div class="col-sm-8 mx-auto">
            <div class="card">
                <div class="card-body">
                    @include('form-f.shared.form-title')
                    <div class="row">
                        <div class="col-sm-12">
                            <form name="formF" ng-controller="FormfFormController" ng-submit="onSubmit($event)" ng-init='formd = @json($form); params = @json($params)'>
                               <div class="row  mt-2">
                                   <div class="col-sm-12">
                                       <div class="float-right">
                                           <strong>Certificate Number:</strong> @{{form.certificate_number}}
                                       </div>
                                   </div>
                               </div>
                               <div class="row mt-2">
                                   <div class="col-sm-12">
                                       <div class="float-right">
                                           <strong>Date of issue:</strong> @{{form.date_of_issue}}
                                       </div>
                                   </div>
                               </div>
                               <div class="row mt-2">
                                   <div class="col-sm-12">
                                       <div class="float-right">
                                           <strong>Valid upto:</strong> @{{form.valid_upto}}
                                       </div>
                                   </div>
                               </div>
                        
                                <div class="row mt-2">
                                    <div class="col-sm-12">
                                        M/S.<strong>@{{formd.concern}}</strong> is hereby given the certificate for manufacture of the Bio-Fertilizer specifies below subject to the terms and conditions of this certificate and to the provisions of the Fertilizer (Control) Order, 1985.
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-sm-6">
                                        <div><strong>Full particulars of the {{strtoupper($form->owner->role->name)}}</strong></div>
                                        <div>@{{formd.particulars}}</div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div><strong>Full Address of the premises where the {{strtoupper($form->owner->role->name)}} will be made</strong></div>
                                        <div>@{{formd.premises}}</div>
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-sm-6">
                                        <div><strong>Date: </strong>@{{form.submit_date}}</div>
                                        <div>
                                        <md-input-container class="md-block">
                                    <label>Place</label>
                                    <input type="text" required name="place"
                                        ng-model="form.place" place>
                                        <div ng-messages="formF.certificate_number.$error">
                                        <div ng-message="required">Place is required</div>
                                    </div>
                                </md-input-container>
                                        </div>
                                    </div>
                                   

                                 </div>
                                 <div class="row">
                                 <div class="col-sm-4 offset-sm-8">
                                    <img ng-if="form.signature" class="signature" ng-src="@{{form.signature}}" alt="">
                                    <md-button ng-if="!form.signature"
                                        class="md-raised md-primary btn-md btn-block float-right" type="button"
                                        ngf-pattern="'image/*,application/pdf'" ngf-select="uploadSignatureFile($file)"
                                        ng-model="signature" ngf-multiple="false">Upload Signature</md-button>
                                    <md-button ng-if="form.signature" class="btn-block md-raised md-accent"
                                        ng-click="onRemoveSignatureFile()">Remove Signature</md-button>

                                    </div>
                                 </div>
                                @include('form-f.shared.terms-and-conditions')
                                <div class="row mt-3">
                                    <div class="col-sm-12">
                                        <md-button class="md-raised md-primary float-right" type="submit"
                                            ng-disabled="formF.$invalid || formDisabled()">Submit</md-button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection