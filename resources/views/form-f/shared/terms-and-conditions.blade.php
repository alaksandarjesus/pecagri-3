<div>
                            <h5><span class="text-underline"> Terms and Conditions of this Certificate:</span></h5>
                            <p>1) The holder of this Certificate shall display the original thereof in a
                                conspicuous place upon to the public in a part of the principal’s premises in
                                which business
                                of making the physical / granulated mixture / organic fertilizer /
                                Bio-fertilizer
                                is carried on and also a copy of such certificate in similar manner in every
                                other
                                premises in which that business is carried on. The required number of copies of
                                the certificate shall be obtained on payment of the fees thereof.
                            </p>
                            <p>2) The holder of this certificate shall not keep in the premises in which he
                                carries on
                                the business of making physical/ granulated mixture of bio fertilizers/ organic
                                Fertilizer in respect of which a certificate of registration has not been
                                obtained
                                under the Fertilizers (Control) Order 1985.
                            </p>
                            <p>3) The Holder of this certificate shall comply with the provisions of the
                                Fertilizers
                                (Control) Order, 1985 and the notification, order and direction, issued there
                                under for the time being in force.
                            </p>
                            <p>4) The holder of this certificate shall comply forthwith to the Registering
                                Authority
                                any change in the premises specified to the certificate or any new premises in
                                which he carried on the business of making physical/ granulated mixture/ organic
                                fertilizer/ bio fertilizer and shall produce before the authority the original
                                Certificate and copies thereof so that necessary corrections may be made therein
                                by the authority.
                            </p>
                            <p>5) The holder of this certificate shall ensure that the physical/ granulated
                                mixture/ organic fertilizer/ bio fertilizer in respect of which a certificate of
                                registration
                                has been obtained is prepared by him or by a person having such qualification,
                                as
                                May be prescribed by the State Government, from time to time or by any other
                                person under the direction, supervision and control of the holder of the person
                                Having said qualification.
                            </p>
                            <p>6) The certificate and copies thereof, if any, will be machine numbered and
                                delivered against the signature of the holder thereof or his agent on the carbon
                                copy of the certificate which will be kept intact in the “Certificate Book” by
                                each
                                Registering Authority.
                            </p>
                        </div>