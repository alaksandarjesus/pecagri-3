@extends('layouts.view')

@section('action')

@include('shared.form.action')

@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    @include('form-f.shared.form-title')
                    <div class="row  mt-2">
                        <div class="col-sm-12">
                            <div class="float-right">
                                <strong>Certificate Number:</strong> @{{$form.certificate_number}}
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm-12">
                            <div class="float-right">
                                <strong>Date of issue:</strong> @{{$form.date_of_issue}}
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm-12">
                            <div class="float-right">
                                <strong>Valid upto:</strong> @{{form.valid_upto}}
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm-12">
                            M/S.<strong>@{{formd.concern}}</strong> is hereby given the certificate for manufacture of
                            the Bio-Fertilizer specifies below subject to the terms and conditions of this certificate
                            and to the provisions of the Fertilizer (Control) Order, 1985.
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm-6">
                            <div><strong>Full particulars of the {{strtoupper($form->owner->role->name)}}</strong></div>
                            <div>@{{formd.particulars}}</div>
                        </div>
                        <div class="col-sm-6">
                            <div><strong>Full Address of the premises where the {{strtoupper($form->owner->role->name)}}
                                    will be made</strong></div>
                            <div>@{{formd.premises}}</div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-sm-6">
                            <div><strong>Date: </strong>@{{form.submit_date}}</div>
                            <div>
                                <strong>Place:</strong> {{$form->place}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection