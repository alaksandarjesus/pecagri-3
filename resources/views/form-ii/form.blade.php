@extends('layouts.form-ii')

@section('content')

<div class="container-fluid" ng-controller="FormiiFormController" ng-cloak>
    @if(isset($form) && !empty($form))
    <div ng-init='form = @json($form)'></div>
    @endif
    <div class="row">
        <div class="col-sm-8 mx-auto">
            <div class="card">
                <div class="card-body">
                    @include('form-ii.shared.form-title')
                    <div class="row">
                        <div class="col-sm-12">
                            <form name="formII" ng-submit="onSubmit($event)">
                                <div class="col-lg-12 ml-auto">
                                    <div ng-if="form.applicant_photo" class="float-right">
                                        <div>
                                            <img class="photo-upload" ng-src="@{{form.applicant_photo}}" alt="">
                                        </div>
                                        <div class="text-center">
                                            <md-button class=" md-accent" ng-click="onRemoveApplicantPhotoFile()">Remove
                                                Photo</md-button>
                                        </div>
                                    </div>
                                    <md-button ng-if="!form.applicant_photo" class="btn-md  float-right photo-upload"
                                        type="button" ngf-pattern="'image/*,application/pdf'"
                                        ngf-select="uploadApplicantPhotoFile($file)" ng-model="applicant_photo"
                                        ngf-multiple="false">Upload Applicant Photo</md-button>

                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-6 col-md-6">
                                    <p>To</p>
                                    <p>The Licensing Authority, </p>
                                    <p> Department Of Agriculture & Farmers welfare, </p>
                                    <p>Puducherry.</p>
                                </div>

                                <p class="m-0 strong">1. Full name</p>
                                <md-input-container class="md-block my-0">
                                    <input type="text" required name="name" ng-model="form.name" aria-label="Name">
                                    <div ng-messages="formII.name.$error">
                                        <div ng-message="required">Name is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0 strong">2. Address</p>
                                <md-input-container class="md-block my-0">
                                    <textarea ng-model="form.address" aria-label="Address" required name="address" md-maxlength="150"
                                        rows="3" md-select-on-focus aria-label="Address"></textarea>
                                    <div ng-messages="formII.address.$error">
                                        <div ng-message="required">Address is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0 strong">3. E-Mail</p>
                                <md-input-container class="md-block my-0">
                                    <input type="email" required name="email" ng-model="form.email">
                                    <div ng-messages="formII.email.$error">
                                        <div ng-message="required">Email is required</div>
                                        <div ng-message="email">Email is not in valid format</div>
                                    </div>
                                </md-input-container>
                                <p>2. Whether the application is for
                                    <strong>@include('forms.titles.form-ii')</strong></p>
                                <p class="m-0 strong">i) Complete address of premises.</p>
                                <md-input-container class="md-block my-0">
                                    <textarea ng-model="form.premises_address" aria-label="Premises Address" required name="premisesAddress"
                                        md-maxlength="150" rows="3" md-select-on-focus
                                        aria-label="premisesAddress"></textarea>
                                    <div ng-messages="formII.premisesAddress.$error">
                                        <div ng-message="required">Premises address is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0 strong">ii) Name of insecticides and its registration number

                                </p>
                                <p class="m-0"><small>(enclose copy of certificate of
                                        registration of the insecticide duly certified by the applicant).</small>

                                    <a href="javascript:void(0)" class="float-right"
                                        ng-click="addInsecticideNameRegNoRow($event)">Add row</a></p>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Insecticides Name</th>
                                            <th>Registration Number</th>
                                            <th style="width:50px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in form.insecticide_name_reg_no track by $index">
                                            <td>
                                                <md-input-container class="md-block m-0 no-errors-spacer">
                                                    <input type="text" name="row-@{{$index}}"
                                                        ng-model="row.insecticide_name" aria-label="Insecticide Name">
                                                </md-input-container>
                                            </td>
                                            <td>
                                                <md-input-container class="md-block m-0 no-errors-spacer">
                                                    <input type="text" name="row-@{{$index}}"
                                                        ng-model="row.registration_number"
                                                        aria-label="Registration Number">
                                                </md-input-container>

                                            </td>
                                            <td>
                                                <md-button class="md-icon-button md-accent" ng-if="$index"
                                                    ng-click="removeInsecticideNameRegNoRow($index)">
                                                    <md-icon>delete_outline</md-icon>
                                                </md-button>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p class="m-0 strong">iii) Validity of certificate of registration</p>
                                <small class="d-block">(as on certificate of registration)</small>
                                <md-input-container class="md-block my-0">
                                    <input type="text" required name="validity" ng-model="form.validity">
                                    <div ng-messages="formII.validity.$error">
                                        <div ng-message="required">validity of registration is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0 strong">4. Complete address (Including name of the lane, PIN Code, etc) of
                                    the premises,
                                    where the insecticide(s) Shall be</p>
                                <p class="m-0">a) Stored/stocked</p>
                                <md-input-container class="md-block my-0">
                                    <textarea ng-model="form.store_address" aria-label="Store Address" required name="storeAddress"
                                        md-maxlength="150" rows="5" md-select-on-focus
                                        ></textarea>
                                    <div ng-messages="formII.storeAddress.$error">
                                        <div ng-message="required">Store/Sold address is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0">b) Sold or exhibited for sale or issued for use in Case of commercial
                                    pest control
                                    operations</p>
                                <md-input-container class="md-block my-0">
                                    <textarea ng-model="form.pestControl" required name="pestControl" md-maxlength="150"
                                        rows="5" md-select-on-focus aria-label="pestControl"></textarea>
                                    <div ng-messages="formII.pestControl.$error">
                                        <div ng-message="required">This field is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0 strong">c) Whether any of the above premises is situated in residential
                                    area (undertaking to
                                    be submitted)</p>
                                <md-radio-group ng-model="form.residentArea"
                                    class="md-block d-flex justify-content-start align-items-center">
                                    <md-radio-button value="Yes" class="md-primary">Yes</md-radio-button>
                                    <md-radio-button value="No" class="ml-4"> No </md-radio-button>
                                </md-radio-group>
                                <p class="m-0 strong">d) Whether food articles are also stored in any of the above
                                    premises (undertaking to
                                    be submitted)</p>
                                <md-radio-group ng-model="form.foodArticle"
                                    class="md-block d-flex justify-content-start align-items-center">
                                    <md-radio-button value="Yes" class="md-primary">Yes</md-radio-button>
                                    <md-radio-button value="No" class="ml-4"> No </md-radio-button>
                                </md-radio-group>
                                <p class="m-0 strong">5. a) Qualification of the applicant the technical personnel under
                                    employment of the
                                    applicant.<a href="javascript:void(0)" class="float-right"
                                        ng-click="addNameDesignationQualificationExperience($event)">Add row</a>
                                </p>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Designation</th>
                                            <th>Qualification</th>
                                            <th>Experience</th>
                                            <th style="width:50px;"></th>
                                        </tr>
                                    </thead>
                                    <tbody
                                        ng-repeat="row in form.name_designation_qualification_experience track by $index">
                                        <tr>
                                            <td>
                                                <md-input-container class="md-block m-0 no-errors-spacer">
                                                    <input type="text" name="row-@{{$index}}" ng-model="row.name"
                                                        aria-label="name-@{{i}}">
                                                </md-input-container>
                                            </td>
                                            <td>
                                                <md-input-container class="md-block m-0 no-errors-spacer">
                                                    <input type="text" name="row-@{{$index}}" ng-model="row.designation"
                                                        aria-label="designation-@{{i}}">
                                                </md-input-container>
                                            </td>
                                            <td>
                                                <md-input-container class="md-block m-0 no-errors-spacer">
                                                    <input type="text" name="row-@{{$index}}"
                                                        ng-model="row.qualification" aria-label="qualification-@{{i}}">
                                                </md-input-container>
                                            </td>
                                            <td>
                                                <md-input-container class="md-block m-0 no-errors-spacer">
                                                    <input type="text" name="row-@{{$index}}" ng-model="row.experience"
                                                        aria-label="experience-@{{i}}">
                                                </md-input-container>
                                            </td>
                                            <td>

                                                <md-button class="md-icon-button md-accent" ng-if="$index"
                                                    ng-click="removeNameDesignationQualificationExperience($index)">
                                                    <md-icon>delete_outline</md-icon>
                                                </md-button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p class="m-0 strong">b) whether fulfill minimum qualification as per insecticide rules
                                </p>
                                <md-radio-group ng-model="form.insecticideQualification"
                                    class="md-block d-flex justify-content-start align-items-center">
                                    <md-radio-button value="Yes" class="md-primary">Yes</md-radio-button>
                                    <md-radio-button value="No" class="ml-4"> No </md-radio-button>
                                </md-radio-group>
                                <p class="m-0 strong">6. Training (wherever applicable)</p>
                                <p class="m-0">(a) Name of the training/ course</p>
                                <md-input-container class="md-block my-0">
                                    <input type="text" required name="courseName" ng-model="form.courseName">
                                    <div ng-messages="formII.courseName.$error">
                                        <div ng-message="required">Course Name is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0">(b) Duration of training/ course</p>
                                <md-input-container class="md-block my-0">
                                    <input type="text" required name="durationCourse" ng-model="form.durationCourse">
                                    <div ng-messages="formII.durationCourse.$error">
                                        <div ng-message="required">Duration is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0">(c) Certificate awarded, if any: (Enclose supporting documents below)</p>
                                <md-radio-group ng-model="form.certificateAward"
                                    class="md-block d-flex justify-content-start align-items-center">
                                    <md-radio-button value="Yes" class="md-primary">Yes</md-radio-button>
                                    <md-radio-button value="No" class="ml-4"> No </md-radio-button>
                                </md-radio-group>
                                <p class="m-0 strong">7. In case of manufacture details of facilities for manufacture of
                                    the
                                    insecticide including infrastructure and those mentioned in Chapter VIII of the
                                    insecticides Rules 1971 and the minimum infrastructure guidelines provided by
                                    the Registration Committee. </p>
                                <p class="m-0 small"> (Enclose complete details in a separate sheet
                                    duly signed by the applicant)</p>
                                <md-radio-group ng-model="form.manufactureDetails"
                                    class="md-block d-flex justify-content-start align-items-center">
                                    <md-radio-button value="Yes" class="md-primary">Yes</md-radio-button>
                                    <md-radio-button value="No" class="ml-4"> No </md-radio-button>
                                </md-radio-group>
                                <p class="m-0 strong">8. In case of application for commercial pest control operations.
                                </p>
                                <p class="m-0 small">(Enclose supporting documents)</p>
                                <md-radio-group ng-model="form.commercialApplication"
                                    class="md-block d-flex justify-content-start align-items-center">
                                    <md-radio-button value="Yes" class="md-primary">Yes</md-radio-button>
                                    <md-radio-button value="No" class="ml-4"> No </md-radio-button>
                                </md-radio-group>
                                <p class="m-0">(a) address of registered, zonal and branch offices</p>
                                <md-input-container class="md-block my-0">
                                    <textarea ng-model="form.zonalBranchAddress" required name="zonalBranchAddress"
                                        md-maxlength="150" rows="5" md-select-on-focus
                                        aria-label="zonalBranchAddress"></textarea>
                                    <div ng-messages="formII.zonalBranchAddress.$error">
                                        <div ng-message="required">Address is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0">(b) address of premises for which the license is applied for</p>
                                <md-input-container class="md-block my-0">
                                    <textarea ng-model="form.premisesLicenceAddress" required
                                        name="premisesLicenceAddress" md-maxlength="150" rows="5" md-select-on-focus
                                        aria-label="premisesLicenceAddress"></textarea>
                                    <div ng-messages="formII.premisesLicenceAddress.$error">
                                        <div ng-message="required">Address is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0">(c) whether approval of technical expertise obtained</p>
                                <md-radio-group ng-model="form.technicalExpertise"
                                    class="md-block d-flex justify-content-start align-items-center">
                                    <md-radio-button value="Yes" class="md-primary">Yes</md-radio-button>
                                    <md-radio-button value="No" class="ml-4"> No </md-radio-button>
                                </md-radio-group>
                                <p class="m-0">(d) If yes, state reference number of approval, its date and validity</p>
                                <md-input-container class="md-block my-0">
                                    <input type="text" required name="refApprovalNumber"
                                        ng-model="form.refApprovalNumber">
                                    <div ng-messages="formII.refApprovalNumber.$error">
                                        <div ng-message="required">Reference no. is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0">Issue Date</p>
                                <md-input-container class="md-block my-0">
                                    <input type="text" required name="itsIssueDate" ng-model="form.itsIssueDate">
                                    <div ng-messages="formII.itsIssueDate.$error">
                                        <div ng-message="required">Issue date is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0">Validity Date</p>
                                <md-input-container class="md-block my-0">
                                    <input type="text" required name="itsValidityDate" ng-model="form.itsValidityDate">
                                    <div ng-messages="formII.itsValidityDate.$error">
                                        <div ng-message="required">Validity date is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0 strong">(e) Name of restricted insecticides for which approved</p>
                                <p class="m-0">Restricted Insecticides</p>
                                <md-input-container class="md-block my-0">
                                    <input type="text" required name="nameRestrict" ng-model="form.nameRestrict">
                                    <div ng-messages="formII.nameRestrict.$error">
                                        <div ng-message="required">Restricted insecticides is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0 strong">(f) name of the responsible technical person</p>
                                <p class="m-0">Technical Person Name</p>
                                <md-input-container class="md-block my-0">
                                    <input type="text" required name="techPersonName" ng-model="form.techPersonName">
                                    <div ng-messages="formII.techPersonName.$error">
                                        <div ng-message="required">Technical Person name is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0 strong">(g) Whether any quantity of restricted insecticide in possession
                                    as on date of
                                    application</p>
                                <md-radio-group ng-model="form.insecticideRestrict"
                                    class="md-block d-flex justify-content-start align-items-center">
                                    <md-radio-button value="Yes" class="md-primary">Yes</md-radio-button>
                                    <md-radio-button value="No" class="ml-4"> No </md-radio-button>
                                </md-radio-group>
                                <p class="m-0 strong">(h) If yes particulars and respective quantity of each in
                                    possession</p>
                                <p class="m-0">Particulars of Quantity in each possession </p>
                                <md-input-container class="md-block my-0">
                                    <textarea ng-model="form.particularsQuantity" required name="particularsQuantity"
                                        md-maxlength="150" rows="5" md-select-on-focus
                                        aria-label="particularsQuantity"></textarea>
                                    <div ng-messages="formII.particularsQuantity.$error">
                                        <div ng-message="required">This field is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0 strong">(i) Details of safety equipment, antidotes and all other essential
                                    facilities</p>
                                <md-input-container class="md-block my-0">
                                    <textarea ng-model="form.detailsEquipment" required name="detailsEquipment"
                                        md-maxlength="150" rows="5" md-select-on-focus
                                        aria-label="detailsEquipment"></textarea>
                                    <div ng-messages="formII.detailsEquipment.$error">
                                        <div ng-message="required">Equipment details is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0 strong">9. In case of licence to sell/stock etc, and for commercial post
                                    control operations,
                                    name of the insecticide(s) and its/their manufacturer/importer which the applicant
                                    intends to deal in and status of the principal certificate(s) in the format five
                                    below (Please enclose principle certificate(s) as per format Appended)</p>
                                <div><a href="javascript:void(0)" ng-click="addLicenseToSellStockInsecticide($event)"
                                        class="float-right">Add row</a></div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Particulars of Insecticide</th>
                                            <th>Name of the Manufacturer/ Importer</th>
                                            <th>Number of Certificate of registration</th>
                                            <th>Detailed principal certificate number / date of issue/
                                                validity
                                            </th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in form.license_to_sell_stock_insecticide track by $index">
                                            <td>
                                                <md-input-container class="md-block m-0 no-errors-spacer">
                                                    <input type="text" name="row-@{{$index}}" ng-model="row.particulars"
                                                        aria-label="particulars-@{{i}}">
                                                </md-input-container>
                                            </td>
                                            <td>
                                                <md-input-container class="md-block m-0 no-errors-spacer">
                                                    <input type="text" name="row-@{{$index}}" ng-model="row.name"
                                                        aria-label="name-@{{i}}">
                                                </md-input-container>
                                            </td>
                                            <td>
                                                <md-input-container class="md-block m-0 no-errors-spacer">
                                                    <input type="text" name="row-@{{$index}}"
                                                        ng-model="row.certificate_number"
                                                        aria-label="certificate_number-@{{i}}">
                                                </md-input-container>
                                            </td>
                                            <td>
                                                <md-input-container class="md-block m-0 no-errors-spacer">
                                                    <input type="text" name="row-@{{$index}}" ng-model="row.date"
                                                        aria-label="date-@{{i}}">
                                                </md-input-container>
                                            </td>
                                            <td>
                                                <md-button class="md-icon-button md-accent" ng-if="$index"
                                                    ng-click="removeLicenseToSellStockInsecticide($index)">
                                                    <md-icon>delete_outline</md-icon>
                                                </md-button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <p class="m-0 strong">10. Full particulars of licence(s)</p>
                                <md-input-container class="md-block my-0">
                                    <textarea ng-model="form.particularLicence" required name="particularLicence"
                                        md-maxlength="150" rows="5" md-select-on-focus
                                        aria-label="particularLicence"></textarea>
                                    <div ng-messages="formII.particularLicence.$error">
                                        <div ng-message="required">Particulars of license is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0 strong">If issued in the name of the applicant by any other state in the
                                    area of their
                                    jurisdiction</p>
                                <md-radio-group ng-model="form.otherStateApplicant"
                                    class="md-block d-flex justify-content-start align-items-center">
                                    <md-radio-button value="Yes" class="md-primary">Yes</md-radio-button>
                                    <md-radio-button value="No" class="ml-4"> No </md-radio-button>
                                </md-radio-group>
                                <p class="m-0 strong">11. In case of renewal, please state licence number and date of
                                    grant</p>
                                <md-input-container class="md-block my-0">

                                    <input type="text" required name="stateLicenceNumber"
                                        ng-model="form.stateLicenceNumber">
                                    <div ng-messages="formII.stateLicenceNumber.$error">
                                        <div ng-message="required">license no. is required</div>
                                    </div>
                                </md-input-container>
                                <p class="m-0">Grant Date</p>
                                <md-input-container class="md-block my-0">
                                    <input type="text" required name="grantDate" ng-model="form.grantDate">
                                    <div ng-messages="formII.grantDate.$error">
                                        <div ng-message="required">Grant date is required</div>
                                    </div>
                                </md-input-container>
                                <p class=" strong m-0">12. Particulars of the application fee paid by the applicant</p>
                                <md-input-container class="md-block m-0">
                                    <textarea ng-model="form.ApplicationFee" required name="ApplicationFee"
                                        md-maxlength="150" rows="5" md-select-on-focus
                                        aria-label="ApplicationFee"></textarea>
                                    <div ng-messages="formII.ApplicationFee.$error">
                                        <div ng-message="required">Application Fee details is required</div>
                                    </div>
                                </md-input-container>
                                <div>
                                    <ul class="list-group">
                                        <li ng-repeat="document in form.documents"
                                            class="list-group-item d-flex justify-content-between align-items-center">
                                            <a ng-href="document.url" target="_blank">@{{document.filename}}</a>
                                            <md-button class="md-icon-button float-right" aria-label="Remove"
                                                ng-click="onRemoveFile(document)">
                                                <md-icon class="text-danger">delete_outline</md-icon>
                                            </md-button>
                                        </li>
                                    </ul>
                                    <md-button class="md-raised md-primary btn-md btn-block float-right" type="button"
                                        ngf-pattern="'image/*,application/pdf'" ngf-select="uploadMultipleFiles($files)"
                                        ng-model="documents" ngf-multiple="true">Upload Documents</md-button>
                                </div>
                                <p class="strong m-0">13. Any other relevant information</p>
                                <md-input-container class="md-block m-0">
                                    <textarea ng-model="form.relevantInfo" required name="relevantInfo"
                                        md-maxlength="150" rows="5" md-select-on-focus aria-label="relevantInfo"
                                        value="NILL"></textarea>
                                    <div ng-messages="formII.relevantInfo.$error">
                                        <div ng-message="required">Relevant Information is required</div>
                                    </div>
                                </md-input-container>
                                <div class="col-lg-12 ml-auto">
                                    <div ng-if="form.signature" class="float-right">
                                        <div>
                                            <img class="signature-upload" ng-src="@{{form.signature}}" alt="">
                                        </div>
                                        <div class="text-center">
                                            <md-button class=" md-accent" ng-click="onRemoveSignatureFile()">Remove
                                                Photo</md-button>
                                        </div>
                                    </div>
                                    <md-button ng-if="!form.signature" 
                                        class="btn-md  float-right signature-upload"
                                        type="button" ngf-pattern="'image/*,application/pdf'"
                                        ngf-select="uploadSignatureFile($file)" ng-model="applicant_photo"
                                        ngf-multiple="false">Applicant signature with seal</md-button>

                                </div>
                                <div class="clearfix"></div>
                                @include('form-ii.shared.declaration')
                                <div class="d-flex justify-content-between align-items-end">
                                    <div>
                                        <p class="m-0 strong">Place</p>
                                        <md-input-container class="d-inline-block" style="width:200px">
                                            <input type="text" required name="application_place"
                                                ng-model="form.application_place">
                                            <div ng-messages="formII.application_place.$error">
                                                <div ng-message="required">Place is required</div>
                                            </div>
                                        </md-input-container>
                                        <p class="m-0 strong">Date</p>
                                        <md-input-container class="md-block mt-2">
                                            <input type="text" required name="date" ng-model="form.date" readonly>
                                            <div ng-messages="formII.date.$error">
                                                <div ng-message="required">Date is required</div>
                                            </div>
                                        </md-input-container>
                                    </div>
                                    <div>
                                        <div ng-if="form.signature1" class="float-right">
                                            <div>
                                                <img class="signature-upload" ng-src="@{{form.signature1}}" alt="">
                                            </div>
                                            <div class="text-center">
                                                <md-button class=" md-accent" ng-click="onRemoveSignature1File()">Remove
                                                    Photo</md-button>
                                            </div>
                                        </div>
                                        <md-button ng-if="!form.signature1" class="btn-md  float-right signature-upload"
                                            type="button" ngf-pattern="'image/*,application/pdf'"
                                            ngf-select="uploadSignature1File($file)" ng-model="signature1"
                                            ngf-multiple="false">Applicant signature with seal</md-button>

                                    </div>

                                </div>
                                <div class="row mt-3">
                                    <div class="col-sm-12">
                                        <md-button class="md-raised md-primary float-right" type="submit"
                                            ng-disabled="formII.$invalid || formDisabled()">Submit</md-button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection