@extends('layouts.form-d')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
            <h5>Form II</h5>
        @include('form-ii.table')
            </div>
        </div>
       
        </div>
    </div>
</div>



@endsection