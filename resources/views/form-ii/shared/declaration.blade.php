<div>
    <h4 class="text-center text-underline">Declaration</h4>
    <div class="row">
        <div class="col-sm-4">
            <p class="strong m-0">Name</p>
            <md-input-container class="md-block m-0">
                <input type="text" required name="decName" ng-model="form.decName">
                <div ng-messages="formII.decName.$error">
                    <div ng-message="required">Name is required</div>
                </div>
            </md-input-container>
        </div>
        <div class="col-sm-4">
            <p class="strong m-0">Son/Daughter of</p>
            <md-input-container class="md-block m-0">
                <md-select ng-model="form.guardianAs" name="guardianAs" required>
                    <md-option value="">Pick a value</md-option>
                    <md-option value="s/o">Son of</md-option>
                    <md-option value="d/o">Daughter of</md-option>
                </md-select>
                <div ng-messages="formII.guardianAs.$error">
                    <div ng-message="required">Son/Daughter of is required</div>
                </div>
            </md-input-container>
        </div>
        <div class="col-sm-4">
            <p class="strong m-0">Son/Daughter of name</p>
            <md-input-container class="md-block m-0">
                <input type="text" required name="sonOfname" ng-model="form.sonOfname">
                <div ng-messages="formII.sonOfname.$error">
                    <div ng-message="required">Name is required</div>
                </div>
            </md-input-container>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <p class="strong m-0">Capacity As</p>
            <md-input-container class="md-block m-0">
                <input type="text" required name="capacityAs" ng-model="form.capacityAs">
                <div ng-messages="formII.capacityAs.$error">
                    <div ng-message="required">Capacity is required</div>
                </div>
            </md-input-container>
        </div>
        <div class="col-sm-6">
            <p class="strong m-0">Virtue Of</p>
            <md-input-container class="md-block m-0">
                <input type="text" required name="virtueOf" ng-model="form.virtueOf">
                <div ng-messages="formII.virtueOf.$error">
                    <div ng-message="required">This field is required</div>
                </div>
            </md-input-container>
        </div>
    </div>
    <div>
        a) I <strong>@{{form.decName}}</strong>, @{{form.guardianAs}}, <strong>@{{form.sonOfname}}</strong> do hereby
        solemnly verify that the information
        given in the application and the annexures and statements
        accompanying it is correct and complete to the pest of liable to be cancelled, if any information, or part
        thereof, is found to be wrong, fake or false at any stage or any condition of license is violated.
    </div>
    <p>
        b) I declare that we have adequate space and facilities to stock insecticides. So as to
        maintain their quality on shell.
    </p>
    <p>
        c) I shall not supply insecticide(s) to any distributor or dealer or person who does not
        have adequate space and facilities to stock them so as to maintain their quality on
        shelf under every circumstances. (for application for licence to manufacture)
    </p>
    <p>
        d) I also declare that I shall not take possession of any stock without satisfying
        myself with the quality thereof.
    </p>
    <p>
        e) I undertake that we shall forthwith inform any change in the responsible technical
        person.
    </p>
    <p>
        f) I undertake that we shall forthwith inform any change in principle certificate to the
        licensing officer (not applicable for application for licence to manufacture)
    </p>
    <p>
        g) I further declare that i am making this application in my capacity as @{{form.capacityAs}}

        and that I am competent to make this application and verify it by virtue of @{{form.virtueOf}}

        an attested copy of which is enclosed herewith. I further declare that I shall abide by conditions laid down
        in the license and failure to do so shall render the license liable to cancellation.
    </p>
</div>