@extends('layouts.view')

@section('action')

@include('shared.form.action', ['type' => 'form-ii'])

@endsection

@section('content')

@include('form-ii.shared.view-title')
<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <p>To</p>
                    <p>The Licensing Authority, <br>
                        Department Of Agriculture & Farmers welfare, <br>
                        Puducherry.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <p>Name, address and e-mail address of the applicant</p>
                </div>
                <div class="col-sm-6 col-md-6">
                    <p>{{$form->name}}, <br>{{$form->address}}, <br>{{$form->email}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <p>2. Whether the application is for</p>
                </div>
                <div class="col-sm-6 col-md-6">
                    <strong>{{strtoupper(session()->get('user')->role->name)}}</strong>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection