@extends('layouts.form-iii.form-iii')

@section('content')

<div class="container-fluid" ng-controller="FormiiiFormController" ng-cloak>
    <div class="row">
        <div class="col-sm-8 mx-auto">
            <div class="card">
                <div class="card-body">
                    @include('form-iii.shared.form-title')
                    <div class="row">
                        <div class="col-sm-12">
                            <form name="formIII" ng-controller="FormiiiFormController" ng-submit="onSubmit($event)"
                                ng-init='formii = @json($form); params = @json($params)'>
                                <div class="row  mt-2">
                                    <div class="col-sm-12">
                                        <p>1. License Number: <strong>@{{form.license_number}}</strong></p>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-sm-12">
                                        <p>Licence to {{strtoupper($form->owner->role->name)}} in the
                                            premises situated at <strong>@{{form.premisesAddress}}</strong> is granted
                                            to M/s @{{form.name}}, @{{form.address}}, @{{form.email}}</p>
                                    </div>
                                </div>
                                <div class="row  mt-2">
                                    <div class="col-sm-12">
                                        <p>table</p>
                                    </div>
                                </div>
                                <div class="row  mt-2">
                                    <div class="col-sm-12">
                                        <p>2. The insecticide(s) shall be {{strtoupper($form->owner->role->name)}} under
                                            the direction and supervision of the following expert staff.</p>
                                    </div>
                                </div>
                                <div class="row  mt-2">
                                    <div class="col-sm-12">
                                        <p>a) For {{strtoupper($form->owner->role->name)}} Name(s) and designation of
                                            the expert staff (Insecticide wise, if any)
                                            <strong>table 5a name and designation</strong></p>
                                    </div>
                                </div>
                                <p>3. The licence is subject to such conditions as may be specified in the rules for the
                                    time being in force under the insecticides. Act 1968 as well as the conditions on
                                    the certificate of registration and others as started below</p>
                                <div class="row">
                                    <div class="col-sm-4 offset-sm-8">
                                        <img ng-if="form.signature" class="signature" ng-src="@{{form.signature}}"
                                            alt="">
                                        <md-button ng-if="!form.signature"
                                            class="md-raised md-primary btn-md btn-block float-right" type="button"
                                            ngf-pattern="'image/*,application/pdf'"
                                            ngf-select="uploadSignatureFile($file)" ng-model="signature"
                                            ngf-multiple="false">Upload Signature of the licensing officer seal
                                        </md-button>
                                        <md-button ng-if="form.signature" class="btn-block md-raised md-accent"
                                            ng-click="onRemoveSignatureFile()">Remove Signature</md-button>
                                    </div>
                                </div>
                                @include('form-iii.shared.conditions')
                                <div>
                                    <img ng-if="form.signature1" class="signature1" ng-src="@{{form.signature1}}"
                                        alt="">
                                    <md-button ng-if="!form.signature1"
                                        class="md-raised md-primary btn-md btn-block float-right" type="button"
                                        ngf-pattern="'image/*,application/pdf'" ngf-select="uploadSignature1File($file)"
                                        ng-model="signature1" ngf-multiple="false">Signature of the licensing officer
                                    </md-button>
                                    <md-button ng-if="form.signature1" class="btn-block md-raised md-accent"
                                        ng-click="onRemoveSignature1File()">Remove Signature</md-button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection