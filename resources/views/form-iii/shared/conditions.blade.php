<div>
    <h4 class="text-center">CONDITIONS</h4>
    <p>1. This licence shall be displayed in the prominent place in the premises for which the
        licence is being issued and shall be produced for inspection as and when required by an
        insecticide Inspector, licensing officer or any other officer authorised by the
        Government in this regard</p>
    <p>2. Any change in the name of the expert staff named in the licence shall forthwith be
        reported to the licensing officer.</p>
    <p>3. The licensee shall scrupulously comply with each and every condition of registration
        of the insecticide(s) failing which the licence is liable to be cancelled.</p>
    <p>4. No insecticide shall be sold or exhibited for sale or distributed or issued for use in
        commercial pest control operations except in packages approved by the Registration
        Committee from time to time.</p>
    <p>5. If the licensee wants to manufacture /. Sell / Stock or exhibit for sale or distribute
        / stock and use for commercial pest control operations, any additional insecticide, he
        may apply to the licensing officer for addition in the licence for each such insecticide
        on payment of the prescribed fee.</p>
    <p>6. For pest control operations an application for the renewal of the licence shall be
        made as laid down in sub-rule (3A) of rule 10 of the insecticides Rules, 1971.</p>
    <p>7. The licensee shall comply with the provisions of the Insecticides Act, 1968, and the
        rules made there under for the time being in force.</p>
    <p>8. The licence also authorizes the storage and stocking of insecticide(s) manufactured at
        the licensed premises in the factory premises for sale by way of wholesale dealing by
        the licensee.</p>
    <p>9. The licensee shall maintain the record of date expired insecticides separately in the
        format as per Appendix A.</p>
    <p>10. The licensee shall maintain the record of sale distribution of insecticides in the
        format as per Appendix B and shall submit monthly return to the Licensing Officer.</p>
    <p>11. The licensee shall maintain the stock register for technical and formulated products
        separately as per Appendix C1 and C2. respectively. (For manufacturer only)</p>
    <p>12. The licensee shall submit the monthly return for technical and formulated products
        separately as per Appendix D1 and D2 respectively. (For manufacturer only)</p>
    <p>13. The licensee shall maintain a record of periodical medical examination of persons
        engaged in connection with insecticides as per Appendix E.</p>
    <p>14. All the registers are to be kept under secured custody by the Licensee and shall be
        provided for scrutiny any time to the insecticide Inspector, Licensing officer or any
        other authorised by the Central Government and Or THe State Government.</p>
    <p>15. Any other condition(s) as specified by the licensing officer.
        <md-input-container class="md-block">
            <textarea ng-model="form.otherConditions" required name="otherConditions" md-maxlength="150"
                rows="5" md-select-on-focus aria-label="otherConditions"></textarea>
            <div ng-messages="formII.otherConditions.$error">
                <div ng-message="required">Other COnditions address is required</div>
            </div>
        </md-input-container>
    </p>
</div>