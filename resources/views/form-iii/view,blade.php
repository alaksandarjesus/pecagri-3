@extends('layouts.view')

@section('action')

@include('shared.form.action')

@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    @include('form-iii.shared.form-title')
                    <div class="row  mt-2">
                        <div class="col-sm-12">
                            <p>1. License Number: <strong>@{{form.license_number}}</strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection