@extends('layouts.forms')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4>Forms</h4>
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered">
                                @if($user->can_apply_form_d)
                                <tr>
                                    <td>
                                        <h5>Form D</h5>
                                      <p>FORM OF APPLICATION TO OBTAIN A CERTIFICATE OF MANUFACTURE OF PHYSICAL/GRANULATED MIXTURE OF FERTILIZER OR ORGANIC FERTILIZER/BIOFERTILIZER</p>
                                    </td>
                                    <td>
                                        <md-button class="md-button" md-no-ink href="{{url('form-d/create')}}">Apply now</md-button>
                                    </td>
                                </tr>
                                @endif
                                @if($user->can_apply_form_ii)
                                <tr>
                                    <td>
                                        <h5>Form II</h5>
                                        @include('forms.titles.form-ii')
                                    </td>
                                    <td>
                                        <md-button class="md-button" md-no-ink href="{{url('form-ii/create')}}">Apply now</md-button>
                                    </td>
                                </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection