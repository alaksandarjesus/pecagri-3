@if(request()->get('user')->role->name === 'Manufacturer Insecticides')

<span>GRANT OF LICENSE TO MANUFACTURE INSECTICIDES</span>

@endif

@if(request()->get('user')->role->name === 'Distributor Insecticides')

<span>GRANT OF LICENSE TO SELL, STOCK OR EXHIBIT FOR SALE OR DISTRIBUTE INSECTICIDES</span>

@endif

@if(request()->get('user')->role->name === 'Pest Control Operations')

<span>GRANT / RENEWAL OF LICENCE TO STOCK AND USE OF INSECTICIDES COMMERCIAL PEST CONTROL OPERATIONS</span>

@endif