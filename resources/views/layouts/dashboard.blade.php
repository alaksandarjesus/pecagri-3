<!DOCTYPE html>
<html lang="en" ng-app="dashboard">
<head>

    @include('shared.layouts.head', array('title' => 'Dashboard'))
    <link rel="stylesheet" href="{{asset('/css/dashboard.css')}}">

</head>
<body>
    @include('shared.navbar.index')
    @yield('content')

    @include('shared.layouts.body')
    
<script src="{{asset('/js/dashboard.js')}}"></script>


</body>
</html>