<!DOCTYPE html>
<html lang="en" ng-app="forma1">
<head>

    @include('shared.layouts.head', array('title' => 'Form A1'))
    <link rel="stylesheet" href="{{asset('/css/form-a1.css')}}">

</head>
<body>
@include('shared.navbar.index')
    
    @yield('content')

    @include('shared.layouts.body')
<script src="{{asset('/js/form-a1.js')}}"></script>
</body>
</html>