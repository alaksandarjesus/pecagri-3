<!DOCTYPE html>
<html lang="en" ng-app="forma2">
<head>

    @include('shared.layouts.head', array('title' => 'Form A2'))
    <link rel="stylesheet" href="{{asset('/css/form-a2.css')}}">

</head>
<body>
@include('shared.navbar.index')
    
    @yield('content')

    @include('shared.layouts.body')
<script src="{{asset('/js/form-a2.js')}}"></script>
</body>
</html>