<!DOCTYPE html>
<html lang="en" ng-app="formd">
<head>

    @include('shared.layouts.head', array('title' => 'Form D'))
    <link rel="stylesheet" href="{{asset('/css/form-d.css')}}">

</head>
<body>
@include('shared.navbar.index')
    
    @yield('content')

    @include('shared.layouts.body')
<script src="{{asset('/js/form-d.js')}}"></script>
</body>
</html>