<!DOCTYPE html>
<html lang="en" ng-app="formf">
<head>

    @include('shared.layouts.head', array('title' => 'Form F'))
    <link rel="stylesheet" href="{{asset('/css/form-f.css')}}">

</head>
<body>
@include('shared.navbar.index')
    
    @yield('content')

    @include('shared.layouts.body')
<script src="{{asset('/js/form-f.js')}}"></script>
</body>
</html>