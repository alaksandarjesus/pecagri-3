<!DOCTYPE html>
<html lang="en" ng-app="formii">
<head>

    @include('shared.layouts.head', array('title' => 'Form II'))
    <link rel="stylesheet" href="{{asset('/css/form-ii.css')}}">

</head>
<body>
@include('shared.navbar.index')
    
    @yield('content')

    @include('shared.layouts.body')
<script src="{{asset('/js/form-ii.js')}}"></script>
</body>
</html>