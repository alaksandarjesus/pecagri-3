<!DOCTYPE html>
<html lang="en" ng-app="formiii">
<head>

    @include('shared.layouts.head', array('title' => 'Form III'))
    <link rel="stylesheet" href="{{asset('/css/form-iii.css')}}">

</head>
<body>
@include('shared.navbar.index')
    
    @yield('content')

    @include('shared.layouts.body')
<script src="{{asset('/js/form-iii.js')}}"></script>
</body>
</html>