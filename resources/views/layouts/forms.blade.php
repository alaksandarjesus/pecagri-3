<!DOCTYPE html>
<html lang="en" ng-app="forms">
<head>

    @include('shared.layouts.head', array('title' => 'Forms'))
    <link rel="stylesheet" href="{{asset('/css/forms.css')}}">

</head>
<body>
@include('shared.navbar.index')
    
    @yield('content')

    @include('shared.layouts.body')
<script src="{{asset('/js/forms.js')}}"></script>
</body>
</html>