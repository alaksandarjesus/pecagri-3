<!DOCTYPE html>
<html lang="en" ng-app="user">
<head>

    @include('shared.layouts.head', array('title' => 'User'))
    <link rel="stylesheet" href="{{asset('/css/user.css')}}">

</head>
<body>
    
    @yield('content')

    @include('shared.layouts.body')
<script src="{{asset('/js/user.js')}}"></script>
</body>
</html>