<!DOCTYPE html>
<html lang="en" ng-app="view">
<head>

    @include('shared.layouts.head')

    <link rel="stylesheet" href="{{asset('/css/view.css')}}">

</head>
<body>
    
 

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @yield('action')
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                @yield('content')
            </div>
        </div>
    </div>


    @include('shared.layouts.body')

<script src="{{asset('/js/view.js')}}"></script>

</body>
</html>