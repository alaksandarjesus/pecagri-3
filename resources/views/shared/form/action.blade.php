<div class="row" ng-controller="ActionController" ng-cloak>
    <div class="col-sm-12">
        <div class="float-right">
        @if($form->can_process)
<md-button class="md-icon-button" ng-click="approveForm($event, '{{$type}}', '{{$form->uuid}}')">
<md-tooltip md-direction="bottom">Approve</md-tooltip>
    <md-icon class="text-success">check</md-icon>
</md-button>
<md-button class="md-icon-button" ng-click="editForm($event, '{{$type}}', '{{$form->uuid}}')">
<md-tooltip md-direction="bottom">Request to edit</md-tooltip>
    <md-icon class="text-warning">edit</md-icon>
</md-button>
<md-button class="md-icon-button" ng-click="rejectForm($event, '{{$type}}', '{{$form->uuid}}')">
<md-tooltip md-direction="bottom">Reject</md-tooltip>
    <md-icon class="text-danger">close</md-icon>
</md-button>
@endif
<md-button class="md-icon-button" ng-click="printForm($event)">
<md-tooltip md-direction="bottom">Print</md-tooltip>
    <md-icon class="text-info">print</md-icon>
</md-button>
        </div>
    </div>
</div>
