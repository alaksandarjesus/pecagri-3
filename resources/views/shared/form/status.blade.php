
@if(!empty($verified_at))
    @if(empty($reject_reason))
    <div>Approved</div>
    <div class="float-right"><small>{{$verified_at}}</small></div>
    @else
    <div>{{$reject_reason}}</div>
    <div class="float-right"><small>{{$verified_at}}</small></div>
    @endif
@endif