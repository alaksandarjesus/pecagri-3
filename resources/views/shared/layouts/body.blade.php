<div ng-controller="AppController">
<div class="api-backdrop" ng-if="apiBackdrop">
      <md-progress-circular md-mode="indeterminate"></md-progress-circular>
    </div>
</div>

<script>
window.APP = {
    API: "{{url('/api')}}"

}
</script>

<script src="{{asset('/js/app.js')}}"></script>
