<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    @if(isset($title) && !empty($title))
    <title>{{$title}}</title>
    @else
    <title>Agriculture</title>
    @endif

    <link rel="stylesheet" href="{{asset('/css/app.css')}}">
