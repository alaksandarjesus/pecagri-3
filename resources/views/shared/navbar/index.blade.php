<md-toolbar class="md-primary" ng-controller="NavbarController">
    <div class="md-toolbar-tools" ng-cloak>
        <md-button class="md-icon-button" aria-label="Settings" ng-disabled="true">
            <md-icon></md-icon>
        </md-button>

        <h2 flex md-truncate>Agriculture Department</h2>
        <md-button href="{{url('forms')}}" class="md-button" aria-label="Forms">
            Forms
        </md-button>

        <md-menu>
            <md-button ng-click="openMenu($mdMenu, $event)" class="md-icon-button" aria-label="User">
                <md-icon>person_outline</md-icon>
            </md-button>
            <md-menu-content>
                <md-menu-item>
                    <md-button href="{{url('dashboard')}}" class="md-button" aria-label="Dashboard">
                        Dashboard
                    </md-button>
                </md-menu-item>
                <md-menu-item>
                    <md-button href="{{url('logout')}}" class="md-button" aria-label="Logout">
                        Logout
                    </md-button>
                </md-menu-item>
            </md-menu-content>
        </md-menu>
    </div>
</md-toolbar>