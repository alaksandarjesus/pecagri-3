@extends('layouts.user')

@section('content')
<div class="container-fluid login-bg">
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-4 mx-auto">
                <md-card md-theme="default" ng-controller="LoginController" ng-cloak>
                    <md-card-title>
                        <md-card-title-text>
                            <span class="md-headline">Login</span>
                        </md-card-title-text>
                    </md-card-title>
                    <md-card-content>
                        <form name="loginForm" ng-submit="onSubmit($event)">
                        <md-input-container class="md-block">
        <label>Select a user</label>
        <md-select ng-model="form.user" ng-init='users=@json($users)' ng-change="onUserChange()">
          <md-option ng-value='@{{user}}' ng-repeat="user in users">@{{user.name}}</md-option>
        </md-select>
      </md-input-container>
                            <md-input-container class="md-block mt-4">
                                <label>Email / Mobile</label>
                                <input type="text" required name="username" ng-model="form.username" autocomplete="username">
                                <div ng-messages="loginForm.username.$error">
                                    <div ng-message="required">Email/Mobile is required.</div>
                                </div>
                            </md-input-container>
                            <md-input-container class="md-block">
                                <label>Password</label>
                                <input type="password" required name="password" ng-model="form.password" autocomplete="current-password">
                                <div ng-messages="loginForm.password.$error">
                                    <div ng-message="required">Password is required.</div>
                                </div>
                            </md-input-container>
                            <div class="row">
                                <div class="col-sm-12">
                                <md-button type="submit" class="md-raised md-primary float-right" ng-disabled="loginForm.$invalid">Submit</md-button>
                                </div>
                            </div>
                        </form>
                </md-card>
            </md-content>
        </div>
    </div>
</div>

@endsection
