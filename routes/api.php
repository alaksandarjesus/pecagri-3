<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'User\LoginController@login');

Route::post('upload/{foldername}', 'UploadController@upload');
Route::post('file-remove', 'UploadController@remove');

Route::group(['prefix' => 'form-d'], function(){
    Route::post('', 'FormDController@store');
    Route::post('process', 'FormDProcessController@process');
});

Route::group(['prefix' => 'form-f'], function(){
    Route::post('', 'FormFController@store');
});