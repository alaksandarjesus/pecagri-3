<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'User\LoginController@view');
Route::get('/logout', 'User\LoginController@logout');
Route::get('dashboard', 'DashboardController@view');
Route::get('forms', 'FormsController@view');

Route::group(['prefix' => 'form-d'], function(){
    Route::get('', 'FormDController@list');
    Route::get('create', 'FormDController@create');
    Route::get('{uuid}/edit', 'FormDController@edit');
    Route::get('{uuid}', 'FormDController@view');
    Route::get('{uuid}/form-f', 'FormFController@create');
    Route::get('{uuid}/form-f/view', 'FormFController@view');

});

Route::group(['prefix' => 'form-ii'], function(){
    Route::get('', 'FormIIController@list');
    Route::get('create', 'FormIIController@create');
    Route::get('{uuid}/edit', 'FormIIController@edit');
    Route::get('{uuid}', 'FormIIController@view');
    Route::get('{uuid}/form-iii', 'FormIIIController@create');
    Route::get('{uuid}/form-iii/view', 'FormIIIController@view');

});

Route::group(['prefix' => 'form-a1'], function(){
    Route::get('', 'FormA1Controller@list');
    Route::get('create', 'FormA1Controller@create');
    Route::get('{uuid}/edit', 'FormA1Controller@edit');
    Route::get('{uuid}', 'FormA1Controller@view');
    Route::get('{uuid}/form-a2', 'FormA2Controller@create');
    Route::get('{uuid}/form-a2/view', 'FormA2Controller@view');

});
