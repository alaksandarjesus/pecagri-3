const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .js('resources/js/user.js', 'public/js')
    .sass('resources/sass/user.scss', 'public/css')
    .js('resources/js/dashboard.js', 'public/js')
    .sass('resources/sass/dashboard.scss', 'public/css')
    .js('resources/js/forms.js', 'public/js')
    .sass('resources/sass/forms.scss', 'public/css')
    .js('resources/js/form-d.js', 'public/js')
    .sass('resources/sass/form-d.scss', 'public/css')
    .js('resources/js/form-f.js', 'public/js')
    .sass('resources/sass/form-f.scss', 'public/css')
    .js('resources/js/form-a1.js', 'public/js')
    .sass('resources/sass/form-a1.scss', 'public/css')
    .js('resources/js/form-a2.js', 'public/js')
    .sass('resources/sass/form-a2.scss', 'public/css')
    .js('resources/js/form-ii.js', 'public/js')
    .sass('resources/sass/form-ii.scss', 'public/css')
    .js('resources/js/form-iii.js', 'public/js')
    .sass('resources/sass/form-iii.scss', 'public/css')
    .js('resources/js/view.js', 'public/js')
    .sass('resources/sass/view.scss', 'public/css')
    .options({
        processCssUrls: false,
    });
    ;


  